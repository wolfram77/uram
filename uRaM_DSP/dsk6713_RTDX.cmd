/**************************************************************/
/*  File dsk6713_RTDX.cmd                                     */
/*    This linker command file can be used as the starting    */
/*  point for linking programs for the TMS320C6713 DSK.       */
/*                                                            */
/*     This command file includes support for RTDX.           */
/*                                                            */
/*  This CMD file assumes everything fits into internal RAM.  */
/*  If that's not true, map some sections to the external     */
/*  SDRAM.                                                    */
/**************************************************************/

-c
-heap  0x1000
-stack 0x1000
-lrts6700.lib
-lcsl6713.lib

_HWI_Cache_Control = 0;

/* RTDX Interrupt Mask
   - This symbol defines those interrupts which are clients of RTDX
     (ie - interrupts which call RTDX functions.
   - RTDX will apply this mask to the IER before excuting code in 
     RTDX critical sections temporarily disabling those interrupts for 
     a few cycles.
   - Any interrupt handlers which call RTDX_read/write functions should 
     be added to the mask to prevent corruption of the RTDX global state
     variables by simulataneous access from multiple RTDX clients.
*/     
_RTDX_interrupt_mask = ~0x000001808;

MEMORY
{
  VECS:  origin = 0x0,         len = 0x00000200   /* interrupt vectors     */
  PMEM:  origin = 0x00000200,  len = 0x0000FE00   /* Internal RAM (L2) mem */
  BMEM:  origin = 0x80000000,  len = 0x01000000   /* CE0, SDRAM, 16 MBytes */
  FLASH: origin = 0x90000000,  len = 0x40000      /* 256 Kbytes flash mem  */
}


SECTIONS
{
  .intvecs:   load = 0x00000000 /* Interrupt vectors included */
                                /*     by using intr_reset()  */ 
  .text:       load = BMEM      /* Executable code            */ 
  .rtdx_text:  load = BMEM
  .const:      load = BMEM      /* Initialized constants      */
  .bss:        load = BMEM      /* Global and static variables*/
  .data:       load = BMEM      /* Data from .asm programs    */
  .rtdx_data:  load = BMEM
  .cinit:      load = BMEM      /* Tables for initializing    */
                                /*    variables and constants */
  .pinit:      load = BMEM
  .stack:      load = BMEM      /* Stack for local variables   */
  .far:        load = BMEM      /* Global and static variables */
                                /* declared far */
  .sysmem:     load = BMEM      /* Used by malloc, etc. (heap) */
  .cio:        load = BMEM      /* Used for C I/O functions    */
  .csldata     load = BMEM
  .switch      load = BMEM
}  
