/*
 *  Copyright(c) 2013 by Subhajit Sahu.
 *  All rights reserved. Property of Subhajit Sahu.
 */

/*
 *  ======== dsk_lib.h ========
 *
 *  Include this file to start working with dsk_lib
 */

#ifndef _dsk_lib_h_
#define _dsk_lib_h_

// Define compile time settings
#define	CHIP_6713		1

// include requisite parts
#include "dsk_lib_tms6713_includes.h"
#include "dsk_lib_tms6713_defines.h"
//#include "dsk_lib_tms6713_routines.h"

// include the C6713 DSK init code
//#include "dsk_lib_tms6713_routines_c.h"
#include "inc/c6713dskinit_src.h"

#endif
