/*
 *  Copyright(c) 2013 by Subhajit Sahu.
 *  All rights reserved. Property of Subhajit Sahu.
 */

/*
 *  ======== dsk_lib_tms6713_defines.h ========
 *
 *  Contains most of the definitions required with dsk_lib
 */

#ifndef _dsk_lib_tms6713_defines_h_
#define _dsk_lib_tms6713_defines_h_

// Global constants
#define	DSK_EMIF_CE1				(0x90080000)
#define	DSK_CPLD_BASEMEM			DSK_EMIF_CE1
#define	DSK_CPLD_USER_REG			(0 + DSK_EMIF_CE1)
#define	DSK_CPLD_DC_REG				(1 + DSK_EMIF_CE1)
#define	DSK_CPLD_VERSION			(4 + DSK_EMIF_CE1)
#define	DSK_CPLD_MISC				(6 + DSK_EMIF_CE1)

#define	DSK_OFF						FALSE
#define	DSK_ON						TRUE

#define	DSK_BOOL				Int16
#define	DSK_BYTE				char
#define	DSK_UBYTE				unsigned char
#define	DSK_SHORT				Int16
#define	DSK_USHORT				Uint16
#define	DSK_INT					Int32
#define	DSK_UINT				Uint32
#define	DSK_CODECHANDLE			DSK6713_AIC23_CodecHandle
#define	DSK_CODECCONFIG			DSK6713_AIC23_Config
#define	DSK_LINEIN				0
#define	DSK_MICIN				1

typedef struct _DSK_SAMPLE_LR
{
	Int16	Right;
	Int16	Left;
}DSK_SAMPLE_LR;

typedef union _DSK_SAMPLE
{
	Uint32 			FullValue;
	DSK_SAMPLE_LR	Value;
}DSK_SAMPLE;


#endif
