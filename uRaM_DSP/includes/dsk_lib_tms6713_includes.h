/*
 *  Copyright(c) 2013 by Subhajit Sahu.
 *  All rights reserved. Property of Subhajit Sahu.
 */

/*
 *  ======== dsk_lib_tms_6713_includes.h ========
 *
 *  Includes most necessary files required for programming with
 *	TMS320C6713DSK kit
 */

#ifndef _dsk_lib_tms_6713_includes_h_
#define _dsk_lib_tms_6713_includes_h_

// include standard C/C++ headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <iostream>
//#include <queue>

// include 6713 related files
#include "inc/csl.h"
#include "inc/csl_chip.h"
#include "inc/csl_dat.h"
#include "inc/csl_edma.h"
#include "inc/csl_mcbsp.h"
#include "inc/dsk6713.h"
#include "inc/dsk6713_aic23.h"
#include "inc/dsk6713_dip.h"
#include "inc/dsk6713_flash.h"
#include "inc/dsk6713_led.h"
#include "inc/rtdx.h"
#include "inc/rtdx_access.h"
#include "inc/rtdx_evt.h"
#include "inc/rtdxpoll.h"
#include "inc/DSKintr.h"
#include "inc/regs.h"

#endif
