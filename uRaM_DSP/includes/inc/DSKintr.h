/******************************************************************************/
/*  DSKint.h                                                                  */
/*                                                                            */
/*     This file provides the header for the DSP's interrupt support.         */
/*                                                                            */
/*  MACRO FUNCTIONS:                                                          */
/*     INTR_GLOBAL_ENABLE() - Enable global interrupts (GIE)                  */
/*     INTR_GLOBAL_DISABLE()- Disable global interrupts (GIE)                 */
/*     INTR_ENABLE()        - Enable interrupt (set bit in IER)               */
/*     INTR_DISABLE()       - Disable interrupt (clear bit in IER)            */
/*     INTR_CHECK_FLAG()    - Check interrupt bit in IFR                      */
/*     INTR_SET_FLAG()      - Set interrupt by writing to ISR bit             */
/*     INTR_CLR_FLAG()      - Clear interrupt by writing to ICR bit           */
/*     INTR_SET_MAP()       - Map CPU interrupt to interrupt selector         */
/*     INTR_GET_ISN()       - Get ISN of selected interrupt                   */
/*     INTR_MAP_RESET()     - Reset interrupt multiplexer map to defaults     */
/*     INTR_EXT_POLARITY()  - Assign external interrupt's polarity            */
/*                                                                            */
/*  FUNCTIONS:                                                                */
/*     intr_reset()         - Reset interrupt registers to default values     */
/*     intr_init()          - Interrupt initialization                        */
/*     intr_isn()           - Assign ISN to CPU interrupt                     */
/*     intr_get_cpu_intr()  - Return CPU interrupt assigned to ISN            */
/*     intr_map()           - Place ISN in interrupt multiplexer register     */
/*                                                                            */
/******************************************************************************/

/*----------------------------------------------------------------------------*/
/* INCLUDES                                                                   */
/*----------------------------------------------------------------------------*/
#include "regs.h"

/*----------------------------------------------------------------------------*/
/* DEFINES AND MACROS                                                         */
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/********************** INTERRUPT SELECTOR REGISTERS **************************/
#define INTR_MULTIPLEX_HIGH_ADDR     0x019C0000
#define INTR_MULTIPLEX_LOW_ADDR      0x019C0004
#define EXTERNAL_INTR_POL_ADDR       0x019C0008

#define INTSEL_SZ           5
#define INTSEL4             0
#define INTSEL5             5
#define INTSEL6            10
#define INTSEL7            16
#define INTSEL8            21
#define INTSEL9            26
#define INTSEL10            0
#define INTSEL11            5  /* Reserved for RTDX */
#define INTSEL12           10  /* Reserved for RTDX */
#define INTSEL13           16
#define INTSEL14           21
#define INTSEL15           26

/* External Interrupt Polarity Register                                       */

#define XIP4                0
#define XIP5                1
#define XIP6                2
#define XIP7                3

/* CPU Interrupt Numbers                                                      */

#define CPU_INT_RST          0x00
#define CPU_INT_NMI          0x01
#define CPU_INT_RSV1         0x02
#define CPU_INT_RSV2         0x03
#define CPU_INT4             0x04
#define CPU_INT5             0x05
#define CPU_INT6             0x06
#define CPU_INT7             0x07
#define CPU_INT8             0x08
#define CPU_INT9             0x09
#define CPU_INT10            0x0A
#define CPU_INT11            0x0B
#define CPU_INT12            0x0C
#define CPU_INT13            0x0D
#define CPU_INT14            0x0E
#define CPU_INT15            0x0F

/* Interrupt Selection Numbers                                                */

#define ISN_DSPINT           0x00
#define ISN_TINT0            0x01
#define ISN_TINT1            0x02
#define ISN_SD_INT           0x03      
#define ISN_GPINT4           0x04
#define ISN_GPINT5           0x05
#define ISN_GPINT6           0x06
#define ISN_GPINT7           0x07
#define ISN_EDMAINT          0x08
#define ISN_EMUDTDMA         0x09
#define ISN_EMURTDXRX        0x0A
#define ISN_EMURTDXTX        0x0B
#define ISN_XINT0            0x0C
#define ISN_RINT0            0x0D
#define ISN_XINT1            0x0E
#define ISN_RINT1            0x0F
#define ISN_GPINT0           0x10
#define ISN_I2CINT0          0x16
#define ISN_I2CINT1          0x17
#define ISN_AXINT0           0x1C
#define ISN_ARINT0           0x1D
#define ISN_AXINT1           0x1E
#define ISN_ARINT1           0x1F

#define IML_SEL              0x00       /* Interrupt Multiplexer Low Select   */
#define IMH_SEL              0x01       /* Interrupt Multiplexer High Select  */
#define IML_RESET_VAL        0x250718A4
#define IMH_RESET_VAL        0x08202D43

/*----------------------------------------------------------------------------*/
/* MACRO FUNCTIONS                                                            */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* INTR_GLOBAL_ENABLE - enables all masked interrupts by setting the GIE      */
/*              bit (bit 0) in the CSR                                        */
/*----------------------------------------------------------------------------*/
#define INTR_GLOBAL_ENABLE() \
        SET_REG_BIT(CSR, GIE)

/*----------------------------------------------------------------------------*/
/* INTR_GLOBAL_DISABLE - disables all masked interrupts by clearing the GIE   */
/*               (bit 0) in the CSR.                                          */
/*----------------------------------------------------------------------------*/
#define INTR_GLOBAL_DISABLE() \
        RESET_REG_BIT(CSR, GIE)

/*----------------------------------------------------------------------------*/
/* INTR_ENABLE - enable interrupt by setting flag in IER                      */
/*----------------------------------------------------------------------------*/
#define INTR_ENABLE(bit) \
        SET_REG_BIT(IER,bit) 

/*----------------------------------------------------------------------------*/
/* INTR_DISABLE - disable interrupt by clearing flag in IER                   */
/*----------------------------------------------------------------------------*/
#define INTR_DISABLE(bit) \
        RESET_REG_BIT(IER,bit) 

/*----------------------------------------------------------------------------*/
/* INTR_CHECK_FLAG - checks status of indicated interrupt bit in IFR          */
/*----------------------------------------------------------------------------*/
#define INTR_CHECK_FLAG(bit) \
        (IFR & MASK_BIT(bit) ? 1 : 0)

/*----------------------------------------------------------------------------*/
/* INTR_SET_FLAG - manually sets indicated interrupt by writing to ISR        */
/*----------------------------------------------------------------------------*/
#define INTR_SET_FLAG(bit) \
        (ISR = MASK_BIT(bit))

/*----------------------------------------------------------------------------*/
/* INTR_CLR_FLAG - manually clears indicated interrupt by writing 1 to ICR    */
/*----------------------------------------------------------------------------*/
#define INTR_CLR_FLAG(bit) \
        (ICR = MASK_BIT(bit))

/*----------------------------------------------------------------------------*/
/* INTR_SET_MAP  - maps a CPU interrupt specified by intr to the interrupt src*/
/*            specified by val.  Sel is used to select between the low and    */
/*            high interrupt_multiplexer registers.                           */
/*----------------------------------------------------------------------------*/
#define INTR_SET_MAP(intsel,val,sel) \
        (sel ? LOAD_FIELD(INTR_MULTIPLEX_HIGH_ADDR,val,intsel,INTSEL_SZ) : \
               LOAD_FIELD(INTR_MULTIPLEX_LOW_ADDR, val,intsel,INTSEL_SZ ))

/*----------------------------------------------------------------------------*/
/* INTR_GET_ISN - returns the ISN value in the selected Interrupt Multiplexer */
/*                register for the interrupt selected by intsel               */
/*----------------------------------------------------------------------------*/
#define INTR_GET_ISN(intsel,sel) \
        (sel ? GET_FIELD(INTR_MULTIPLEX_HIGH_ADDR,intsel,INTSEL_SZ) : \
               GET_FIELD(INTR_MULTIPLEX_LOW_ADDR, intsel, INTSEL_SZ))

/*----------------------------------------------------------------------------*/
/* INTR_MAP_RESET - resets the interrupt multiplexer maps to their default val*/
/*----------------------------------------------------------------------------*/
#define INTR_MAP_RESET() \
        { REG_WRITE (INTR_MULTIPLEX_HIGH_ADDR,IMH_RESET_VAL); \
          REG_WRITE (INTR_MULTIPLEX_LOW_ADDR, IML_RESET_VAL); }

/*----------------------------------------------------------------------------*/
/* INTR_EXT_POLARITY - assigns external interrupt external priority.          */
/*                    val = 0 (normal), val = 1 (inverted)                    */
/*----------------------------------------------------------------------------*/
#define INTR_EXT_POLARITY(bit,val) \
        (val ? SET_BIT(EXTERNAL_INTR_POL_ADDR,bit) : \
               RESET_BIT(EXTERNAL_INTR_POL_ADDR,bit))


/*----------------------------------------------------------------------------*/
/* GLOBAL VARIABLES                                                           */
/*----------------------------------------------------------------------------*/

extern far void istb();
extern far void NMI(), RESV1(), RESV2();
extern far void unexp_int04(), unexp_int05(), unexp_int06(), unexp_int07();
extern far void unexp_int08(), unexp_int09(), unexp_int10(), unexp_int11();
extern far void unexp_int12(), unexp_int13(), unexp_int14(), unexp_int15();

extern void interrupt c_int00(void);

/*----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                  */
/*----------------------------------------------------------------------------*/

void intr_reset(void);
void intr_init(void);
void intr_hook(void (*fp)(void),int cpu_intr);

