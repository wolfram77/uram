/*
 *  Copyright 2004 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  @(#) Rtdx 5,0,0 11-18-2004 (rtdxtc-a01)
 */
/**************************************************************************
* $RCSFile: /db/sds/syseng/rtdx/target/usrlib/c6x/rtdx_access.h,v $
* $Revision: 1.5 $
* $Date: 2000/07/07 17:28:40 $
* Copyright (c) 2000 Texas Instruments Incorporated
*
* Large Model/FAR mode support
**************************************************************************/
#ifndef __RTDX_ASCCESS_H
#define __RTDX_ASCCESS_H

#ifndef _TMS320C6X
#error "Wrong Target - This code for C6x only!"
#endif


/* C6x always uses FAR mode to avoid MVKL/MVKH issues   */
#define RTDX_FAR_MODE           1
#define RTDX_USE_CODE_SECTION   1
#define RTDX_USE_DATA_SECTION   1

#if RTDX_USE_CODE_SECTION
#define RTDX_CODE       far
#else
#define RTDX_CODE
#endif

#if RTDX_USE_DATA_SECTION
#define RTDX_DATA       far
#else
#define RTDX_DATA
#endif


#endif /* __RTDX_ACCESS_H */
