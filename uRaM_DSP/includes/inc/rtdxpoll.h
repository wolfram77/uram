/*
 *  Copyright 2004 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  @(#) Rtdx 5,0,0 11-18-2004 (rtdxtc-a01)
 */
/***********************************************************************
* $RCSFile: /db/sds/syseng/rtdx/target/usrlib/c6x/rtdx_access.h,v $
* $Revision: 1.2 $
* $Date: 2000/06/30 14:39:03 $
* Copyright (c) 2000 Texas Instruments Incorporated
*
* The RTDX Target Library User Interface
************************************************************************/
#ifndef __RTDXPOLL_H
#define __RTDXPOLL_H

/* This controls configuration to be either polling or interrupt driven */
/* It must be defined - values are 0 or 1.                              */
#undef RTDX_POLLING_IMPEMENTATION
#define RTDX_POLLING_IMPLEMENTATION 0
#if RTDX_POLLING_IMPLEMENTATION
extern void RTDX_Poll( void );
#endif

#endif  /* __RTDXPOLL_H */
