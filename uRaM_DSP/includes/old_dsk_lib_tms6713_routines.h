/*
 *  Copyright(c) 2013 by Subhajit Sahu.
 *  All rights reserved. Property of Subhajit Sahu.
 */

/*
 *  ======== dsk_lib_tms6713_routines.c ========
 *
 *  Contains routines to work with dsk_lib
 */


#ifndef _dsk_lib_tms6713_routines_h_
#define _dsk_lib_tms6713_routines_h_


// Ends the AIC23 codec operation
// - must be used only before ending the program
inline void dsk_EndCodec(void);

// Configures AIC23 codec with the provided register values
// - using this to set the sampling rate will not update the
// - software value of sampling frequency
void dsk_ConfigCodec(DSK_CODECCONFIG *config);

// Configures the AIC23 codec with default settings
// - sampling frequency is set to 48000
inline void dsk_DefaultConfigCodec(void);

// Sets the sampling rate of AIC23 codec
void dsk_SetSamplingRate(DSK_UINT sampling_rate);

// Gives the sampling rate of AIC23 codec
inline DSK_UINT dsk_GetSamplingRate(void);

// Sets the output gain of AIC23 codec
inline void dsk_SetOutputGain(DSK_USHORT outgain);

// Gives the output gain of AIC23 codec
inline DSK_USHORT dsk_GetOutputGain(void);

// Sets the loopback mode of AIC23 codec
inline void dsk_SetLoopbackMode(DSK_SHORT mode);

// Gives the loopback mode of AIC23 codec
inline DSK_SHORT dsk_GetLoopbackMode(void);

// Sets the mute mode of AIC23 codec
inline void dsk_SetMuteMode(DSK_SHORT mode);

// Gives the mute mode of AIC23 codec
inline DSK_SHORT dsk_GetMuteMode(void);

// Sets the powerdown mode of AIC23 codec
inline void dsk_SetPowerdownMode(DSK_USHORT mode);

// Gives the powerdown mode of AIC23 codec
inline DSK_USHORT dsk_GetPowerdownMode(void);

// Reads a sample from AIC23 codec
inline DSK_SAMPLE dsk_ReadCodecSample();

// Writes a sample to AIC23 codec
inline void dsk_WriteCodecSample(DSK_SAMPLE sample);

// Reads a sample from MCBSP
DSK_SAMPLE dsk_ReadMcbspSample(void);

// Writes a sample to MCBSP
void dsk_WriteMcbspSample(DSK_SAMPLE sample);

// Gives the version of the DSP processor
inline DSK_SHORT dsk_GetVersion(void);

// Provides a delay of given microseconds
inline void dsk_DelayUs(DSK_UINT microseconds);

// Provides a delay of given milliseconds
inline void dsk_DelayMs(DSK_UINT milliseconds);

// Gives the value of given DIP switch number
inline DSK_UINT dsk_GetDipSwitchValue(DSK_UINT switch_no);

// Gives the value of Flash checksum of given region
inline DSK_UINT dsk_GetFlashRegionChecksum(DSK_UINT rgn_start, DSK_UINT rgn_length);

// Erases the given region of Flash memory
inline void dsk_EraseFlashRegion(DSK_UINT rgn_start, DSK_UINT rgn_length);

// Reads the given region of Flash memory to destination memory
inline void dsk_ReadFlashRegion(DSK_UINT src_rgn_start, DSK_UINT dest_rgn_start, DSK_UINT rgn_length);

// Writes the given region of Flash memory to destination memory
inline void dsk_WriteFlashRegion(DSK_UINT src_rgn_start, DSK_UINT dest_rgn_start, DSK_UINT rgn_length);

// Sets the given LED in the provided state
inline void dsk_SetLED(DSK_UINT led_number, DSK_BOOL led_on);

// Toggles the given LED
inline void dsk_ToggleLED(DSK_UINT led_number);

// Gives the CPU ID of the processor
inline DSK_UINT dsk_GetCpuId(void);

// Gives the sillicon revision ID of the processor
inline DSK_UINT dsk_GetSilliconRevId(void);

// Gives the endian mode of the processor
inline DSK_UINT dsk_GetEndianMode(void);

// Gives the map mode of the processor
inline DSK_UINT dsk_GetMapMode(void);

// Set the value of a AIC23 Codec register
inline void dsk_SetCodecRegister(DSK_USHORT reg_num, DSK_USHORT reg_val);

// Get the value of a AIC23 Codec register
inline DSK_USHORT dsk_GetCodecRegister(DSK_USHORT reg_num);

// Sets the input source to be used
void dsk_SetInputSource(DSK_USHORT input_source);

// Starts / initializes the 6713 DSK with a desired sampling rate
// - must be called before any other operation in the main
// - program which uses IO operations
void dsk_Start(DSK_UINT sampling_rate);

#endif

