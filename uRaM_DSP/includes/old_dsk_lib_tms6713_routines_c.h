/*
 *  Copyright(c) 2013 by Subhajit Sahu.
 *  All rights reserved. Property of Subhajit Sahu.
 */

/*
 *  ======== dsk_lib_tms6713_routines.c ========
 *
 *  Contains routines to work with dsk_lib
 */


#ifndef _dsk_lib_tms6713_routines_c_h_
#define _dsk_lib_tms6713_routines_c_h_


// Global variables
DSK_CODECHANDLE 			dsk_aic23codechandle;
MCBSP_Handle	 			dsk_mcbspcontrolhandle;
MCBSP_Handle	 			dsk_mcbspdatahandle;
DSK_UINT					dsk_SamplingRate;
DSK_USHORT					dsk_OutputGain;
DSK_SHORT					dsk_LoopbackMode;
DSK_SHORT					dsk_MuteMode;
DSK_USHORT					dsk_PowerdownMode;
DSK_SAMPLE					dsk_Sample;




// 
// DSK_BOOL dsk_GetSwitch(DSK_UINT switch_number) {
// 	return 
// }



// Ends the AIC23 codec operation
// - must be used only before ending the program
inline void dsk_EndCodec(void) {
	DSK6713_AIC23_closeCodec(dsk_aic23codechandle);
}

// Configures AIC23 codec with the provided register values
// - using this to set the sampling rate will not update the
// - software value of sampling frequency
void dsk_ConfigCodec(DSK_CODECCONFIG *config, MCBSP_Config *mcbspControl, MCBSP_Config *mcbspData) {
	if(!dsk_aic23codechandle) DSK6713_AIC23_openCodec(0, config);
	else DSK6713_AIC23_config(dsk_aic23codechandle, config);
	if(dsk_mcbspcontrolhandle.allocated) MCBSP_close(dsk_mcbspcontrolhandle);
	if(dsk_mcbspcontrolhandle.allocated) MCBSP_close(dsk_mcbspdatahandle);
	dsk_mcbspcontrolhandle = MCBSP_open(MCBSP_DEV1, MCBSP_OPEN_RESET);
	dsk_mcbspdatahandle = MCBSP_open(MCBSP_DEV2, MCBSP_OPEN_RESET);
	MCBSP_config(dsk_mcbspcontrolhandle, &mcbspControl);
	MCBSP_config(dsk_mcbspdatahandle, &mcbspData);
    MCBSP_start(dsk_mcbspcontrolhandle, MCBSP_XMIT_START | MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 100);
	// initCodec(dsk_mcbspcontrolhandle);
    /* Clear any garbage from the codec data port */
    if (MCBSP_rrdy(dsk_mcbspdatahandle)) MCBSP_read(dsk_mcbspdatahandle);
    /* Start McBSP2 as the codec data channel */
    MCBSP_start(dsk_mcbspdatahandle, MCBSP_XMIT_START | MCBSP_RCV_START | MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);
	MCBSP_write(dsk_mcbspdatahandle,0);
}


// Configures the AIC23 codec with default settings
// - sampling frequency is set to 48000
inline void dsk_DefaultConfigCodec(void) {
	DSK_CODECCONFIG config = DSK6713_AIC23_DEFAULTCONFIG;

	dsk_ConfigCodec(&config, &mcbspCfgControl, &mcbspCfgData);
	dsk_SamplingRate = 48000;
}

// Sets the sampling rate of AIC23 codec
void dsk_SetSamplingRate(DSK_UINT sampling_rate) {
	DSK_UINT r;

	switch(sampling_rate)
	{
		default:
		case 8000:
			r = DSK6713_AIC23_FREQ_8KHZ;
			break;
		case 16000:
			r = DSK6713_AIC23_FREQ_16KHZ;
			break;
		case 24000:
			r = DSK6713_AIC23_FREQ_24KHZ;
			break;
		case 32000:
			r = DSK6713_AIC23_FREQ_32KHZ;
			break;
		case 44100:
			r = DSK6713_AIC23_FREQ_44KHZ;
			break;
		case 48000:
			r = DSK6713_AIC23_FREQ_48KHZ;
			break;
		case 96000:
			r = DSK6713_AIC23_FREQ_96KHZ;
			break;
	}
	DSK6713_AIC23_setFreq(dsk_aic23codechandle, r);
	dsk_SamplingRate = r;
}

// Gives the sampling rate of AIC23 codec
inline DSK_UINT dsk_GetSamplingRate(void) {
	return dsk_SamplingRate;
}

// Sets the output gain of AIC23 codec
inline void dsk_SetOutputGain(DSK_USHORT outgain) {
	DSK6713_AIC23_outGain(dsk_aic23codechandle, outgain);
	dsk_OutputGain = outgain;
}

// Gives the output gain of AIC23 codec
inline DSK_USHORT dsk_GetOutputGain(void) {
	return dsk_OutputGain;
}

// Sets the loopback mode of AIC23 codec
inline void dsk_SetLoopbackMode(DSK_SHORT mode) {
	DSK6713_AIC23_loopback(dsk_aic23codechandle, mode);
	dsk_LoopbackMode = mode;
}

// Gives the loopback mode of AIC23 codec
inline DSK_SHORT dsk_GetLoopbackMode(void) {
	return dsk_LoopbackMode;
}

// Sets the mute mode of AIC23 codec
inline void dsk_SetMuteMode(DSK_SHORT mode) {
	DSK6713_AIC23_mute(dsk_aic23codechandle, mode);
	dsk_MuteMode = mode;
}

// Gives the mute mode of AIC23 codec
inline DSK_SHORT dsk_GetMuteMode(void) {
	return dsk_MuteMode;
}

// Sets the powerdown mode of AIC23 codec
inline void dsk_SetPowerdownMode(DSK_USHORT mode) {
	DSK6713_AIC23_powerDown(dsk_aic23codechandle, mode);
	dsk_PowerdownMode = mode;
}

// Gives the powerdown mode of AIC23 codec
inline DSK_USHORT dsk_GetPowerdownMode(void) {
	return dsk_PowerdownMode;
}

// Reads a sample from AIC23 codec
inline DSK_SAMPLE dsk_ReadCodecSample() {
	while(!DSK6713_AIC23_read(dsk_aic23codechandle, (DSK_UINT*) &dsk_Sample));
	return dsk_Sample;
}

// Writes a sample to AIC23 codec
inline void dsk_WriteCodecSample(DSK_SAMPLE sample) {
	while(!DSK6713_AIC23_write(dsk_aic23codechandle, sample.FullValue));
}

// Reads a sample from MCBSP
DSK_SAMPLE dsk_ReadMcbspSample(void) {
	dsk_Sample.FullValue = MCBSP_read(dsk_mcbspdatahandle);
	return dsk_Sample;
}

// Writes a sample to MCBSP
void dsk_WriteMcbspSample(DSK_SAMPLE sample) {
	MCBSP_write(dsk_mcbspdatahandle, sample.FullValue);
}

// Gives the version of the DSP processor
inline DSK_SHORT dsk_GetVersion(void) {
	return DSK6713_getVersion();
}

// Provides a delay of given microseconds
inline void dsk_DelayUs(DSK_UINT microseconds) {
	DSK6713_waitusec(microseconds);
}

// Provides a delay of given milliseconds
inline void dsk_DelayMs(DSK_UINT milliseconds) {
	DSK6713_waitusec(1000 * milliseconds);
}

// Gives the value of given DIP switch number
inline DSK_UINT dsk_GetDipSwitchValue(DSK_UINT switch_no) {
	return DSK6713_DIP_get(switch_no);
}

// Gives the value of Flash checksum of given region
inline DSK_UINT dsk_GetFlashRegionChecksum(DSK_UINT rgn_start, DSK_UINT rgn_length) {
	return DSK6713_FLASH_checksum(rgn_start, rgn_length);
}

// Erases the given region of Flash memory
inline void dsk_EraseFlashRegion(DSK_UINT rgn_start, DSK_UINT rgn_length) {
	DSK6713_FLASH_erase(rgn_start, rgn_length);
}

// Reads the given region of Flash memory to destination memory
inline void dsk_ReadFlashRegion(DSK_UINT src_rgn_start, DSK_UINT dest_rgn_start, DSK_UINT rgn_length) {
	DSK6713_FLASH_read(src_rgn_start, dest_rgn_start, rgn_length);
}

// Writes the given region of Flash memory to destination memory
inline void dsk_WriteFlashRegion(DSK_UINT src_rgn_start, DSK_UINT dest_rgn_start, DSK_UINT rgn_length) {
	DSK6713_FLASH_write(src_rgn_start, dest_rgn_start, rgn_length);
}

// Sets the given LED in the provided state
inline void dsk_SetLED(DSK_UINT led_number, DSK_BOOL led_on) {
	if(led_on) DSK6713_LED_on(led_number);
	else DSK6713_LED_off(led_number);
}

// Toggles the given LED
inline void dsk_ToggleLED(DSK_UINT led_number) {
	DSK6713_LED_toggle(led_number);
}

// Gives the CPU ID of the processor
inline DSK_UINT dsk_GetCpuId(void) {
	return CHIP_getCpuId();
}

// Gives the sillicon revision ID of the processor
inline DSK_UINT dsk_GetSilliconRevId(void) {
	return CHIP_getSiliconRevId();
}

// Gives the endian mode of the processor
inline DSK_UINT dsk_GetEndianMode(void) {
	return CHIP_getEndian();
}

// Gives the map mode of the processor
inline DSK_UINT dsk_GetMapMode(void) {
	return CHIP_getMapMode();
}

// Set the value of a AIC23 Codec register
inline void dsk_SetCodecRegister(DSK_USHORT reg_num, DSK_USHORT reg_val) {
	DSK6713_AIC23_rset(dsk_aic23codechandle, reg_num, reg_val);
}

// Get the value of a AIC23 Codec register
inline DSK_USHORT dsk_GetCodecRegister(DSK_USHORT reg_num) {
	return DSK6713_AIC23_rget(dsk_aic23codechandle, reg_num);
}

// Sets the input source to be used
void dsk_SetInputSource(DSK_USHORT input_source) {
	DSK_USHORT inp = input_source & 1;
	DSK_USHORT old_reg_val;
	old_reg_val = dsk_GetCodecRegister(4);
	old_reg_val &= ~2;
	old_reg_val |= (inp << 2);
	dsk_SetCodecRegister(4, old_reg_val);
}

// Starts / initializes the 6713 DSK with a desired sampling rate
// - must be called before any other operation in the main
// - program which uses IO operations
void dsk_Start(DSK_UINT sampling_rate) {
	// initialize the chip support library
	CSL_init();
	printf("CSL initialized\n");
	
	// initialize for BSL functions
	DSK6713_init();
	printf("DSK6713 initialized\n");

	// initialize DIP switches
	DSK6713_DIP_init();
	printf("DSK6713_DIP initialized\n");
	
	// initialize LEDs
	DSK6713_LED_init();
	printf("DSK6713_LED initialized\n");

	// initialize interrupt system
	// intr_init();
	// intr_reset();
	// printf("Interrupt system initialized\n");
	
	// start AIC32 codec with default settings
	// (for IO operations from mic, line in, spkr, line out)
	dsk_DefaultConfigCodec();
	printf("Codec initialized\n");
	
	// set the desired sampling rate
	dsk_SetSamplingRate(sampling_rate);
	printf("Sampling rate set\n");
}


#endif

