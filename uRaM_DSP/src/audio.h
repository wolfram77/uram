/*
 * audio.h
 *
 *  Created on: 15-Mar-2013
 *   Completed: 15-Mar-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef AUDIO_H_
#define AUDIO_H_

#include "virtuoscope.h"

#ifdef	DSP_PROCESSOR
	#define		audio_Tx_Init(disp_sz)
	#define		audio_Rx_Init(disp_sz)
	#define		audio_Tx(sample, amp)				output_sample((int)(sample * amp))
	#define		audio_Rx(amp)						((float)input_sample() / amp)
	#define		audio_Tx_Close()
	#define		audio_Rx_Close()
#else
	SCOPE		audio_TxScope;
	SCOPE		audio_RxScope;
	#define		audio_Tx_Init(disp_sz)				virtuoscope_Init(&audio_TxScope, "audio_Tx.vso", sizeof(float), disp_sz)
	#define		audio_Rx_Init(disp_sz)				virtuoscope_Init(&audio_RxScope, "audio_Rx.vso", disp_sz, sizeof(float))
	#define		audio_Tx(sample, amp)				virtuoscope_Write(&audio_TxScope, (int)(sample * amp))
	#define		audio_Rx(amp)						virtuoscope_Write(&audio_TxScope, (int)(sample * amp))
	#define		audio_Tx_Close()					virtuoscope_Close(&audio_TxScope)
	#define		audio_Rx_Close()					virtuoscope_Close(&audio_RxScope)
#endif

#endif /* AUDIO_H_ */
