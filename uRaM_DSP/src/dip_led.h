/*
 * dip_led.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef DIP_LED_H_
#define DIP_LED_H_


// prog - LED state = DIP state
void CopyDipToLed()
{
	int i, sw;

	while(1)
	{
		for(i=0; i<4; i++)
		{
			sw = DSK6713_DIP_get(i);
			if(sw) DSK6713_LED_on(i);
			else DSK6713_LED_off(i);
		}
	}
}


#endif /* DIP_LED_H_ */
