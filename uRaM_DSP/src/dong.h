/*
 * dong.h
 *
 *  Created on: 10-Mar-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef DONG_H_
#define DONG_H_

#include "src\maths.h"
#include "src\struct.h"
#include "src\queue.h"
#include "src\interrupt.h"
#include "src\audio.h"

void dong_Init();
void dong_initSamplesPerSymbol();
void dong_initSymbolsPerWord();
void dong_initWordsPerFrame();
void dong_initSymbols();

QUEUE	dong_Tx;
QUEUE	dong_Rx;
int		dong_TxVal;
float	dong_TxAmp			=	30000.0f;
float	dong_RxAmp			=	30000.0f;
float	dong_Fs				=	96000.0f;
float	dong_SymSfreq		=	40000.0f;
float	dong_Sym0freq		=	36000.0f;
float	dong_Sym1freq		=	44000.0f;
float*	dong_SymS;
float*	dong_Sym0;
float*	dong_Sym1;
float	dong_SymCycles		=	20.0f;
int		dong_SymSamples;
int		dong_WordSyms;
float	dong_Tframe			=	0.0066666667f;
int		dong_FrameWords;
float	dong_Vsnd;
float*	dong_RxBufInp;
float*	dong_RxBufCorr;

// ISR for data transmission
interrupt_OutputISR()
{
	float	sample = 0.0f;

	// play data in the transmission queue
	queue_Pop(&dong_Tx, sizeof(float), &sample);
	audio_Tx(sample, dong_TxAmp);
}

// ISR for data reception
interrupt_InputISR()
{
	float	sample = 0.0f;

	// record data to reception queue
	sample = audio_Rx(dong_RxAmp);
	queue_Push(&dong_Rx, sizeof(float), &sample);
}

// initialize
int dong_Init(int suggested_tx_size, int suggested_rx_size)
{
	int		i, WordBytes;
	float	t, ts;
	float	tWord, minFreq, bits, count;

	// samples per symbol
	minFreq = (dong_SymSfreq < dong_Sym0freq)? dong_SymSfreq : dong_Sym0freq;
	minFreq = (dong_Sym1freq < minFreq)? dong_Sym1freq : minFreq;
	dong_SymSamples = (int)ceil((dong_SymCycles * dong_Fs) / minFreq);
	// symbols per word (excluding start symbol)
	bits = 0.0f;
	while(1)
	{
		count = (dong_Tframe * dong_Fs) / (dong_SymSamples * (bits + 1));
		if(count <= pow(2, bits)) break;
		bits += 1.0f;
	}
	dong_WordSyms = (int)bits;
	// words per frame
	tWord = dong_SymSamples * (dong_WordSyms + 1) / dong_Fs;
	dong_FrameWords = (int)(dong_Tframe / tWord);
	// init symbols (start, 0, 1)
	dong_SymS = mem_Alloc(dong_SymSamples, float);
	dong_Sym0 = mem_Alloc(dong_SymSamples, float);
	dong_Sym1 = mem_Alloc(dong_SymSamples, float);
	ts = 1.0f / dong_Fs;
	for(i=0, t=0.0f; i<dong_SymSamples; i++, t+=ts)
	{	dong_SymS[i] = (float)sin(2.0*maths_Pi* dong_SymSfreq * t);
		dong_Sym0[i] = (float)cos(2.0*maths_Pi* dong_Sym0freq * t);
		dong_Sym1[i] = (float)(-sin(2.0*maths_Pi* dong_Sym1freq * t));
	}
	// init Tx, Rx
	WordBytes = (dong_WordSyms + 1) * dong_SymSamples * sizeof(float);
	queue_Init(&dong_Tx, WordBytes, sizeof(float), suggested_tx_size);
	queue_Init(&dong_Rx, sizeof(float), WordBytes, suggested_rx_size);
	// Init Tx value
	dong_TxVal = 0;
	// Init Rx buffers
	// dong_Rx_BufInp = mem_Alloc('
	return 0;
}

int dong_TxWrite()
{
	float*	sym;
	int		i, val, ret, WordBytes, SymBytes;
	
	SymBytes = dong_SymSamples * sizeof(float);
	WordBytes = (dong_WordSyms + 1) * SymBytes;
	ret = queue_Rear(&dong_Tx, WordBytes);
	if(ret < 0) return -1;
	val = dong_TxVal;
	queue_Push(&dong_Tx, SymBytes, dong_SymS);
	for(i=0; i<dong_WordSyms; i++)
	{
		sym = (val & 1 == 0)? dong_Sym0 : dong_Sym1;
		queue_Push(&dong_Tx, SymBytes, sym);
	}
	dong_TxVal = (dong_TxVal + 1) % dong_FrameWords;
	return 0;
}

int dong_RxRead()
{
	int		ret, WordBytes, SymBytes;


	SymBytes = dong_SymSamples * sizeof(float);
	WordBytes = (dong_WordSyms + 1) * SymBytes;
	ret = queue_Front(&dong_Rx, WordBytes<<1, dong_RxBufInp);
	if(ret < 0) return -1;

}








#endif /* DONG_H_ */
