
#define		DELAY_BUFFER_SIZE		1024

short	DelayBuffer[DELAY_BUFFER_SIZE];
short	DelayPtr = 0;
float	Gain = 0.3;
float	OutGain = 20;

void OutputEcho()
{
	int		i;
	short	sample = 0;

	printf("Running OutputEcho\n");
	while(1) //infinite loop
	{
		sample = input_left_sample();
		// printf("%d\n",sample);
		// sample += (short)(Gain * DelayBuffer[DelayPtr]);
		sample = (sample + 1) % 8;
		output_right_sample(sample * 3200);
		// DelayBuffer[DelayPtr] = sample;
		DelayPtr = (DelayPtr >= DELAY_BUFFER_SIZE)? 0 : DelayPtr+1;
		if(DelayPtr == 0)
		{
			for(i=0; i<4; i++)
				DSK6713_LED_toggle(i);
		}
	}
}

