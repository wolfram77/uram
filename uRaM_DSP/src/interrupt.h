/*
 * interrupt.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 14-Mar_2013
 *      Author: Subhajit
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

// of use only in DSP processor code
#define		interrupt_Enable
#define		interrupt_Disable
#ifdef	DSP_PROCESSOR
	#define		interrupt_InputISR			interrupt void c_int11
	#define		interrupt_OutputISR			interrupt void c_int11
#else
	#define		interrupt_InputISR			void InputISR
	#define		interrupt_OutputISR			void OutputISR
#endif

#endif /* INTERRUPT_H_ */
