/*
 * main.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef MAIN_H_
#define MAIN_H_


// settings
#define DSK6713_AIC23_INPUT_MIC					0x0015
#define DSK6713_AIC23_INPUT_LINE				0x0011
Uint16 inputsource	=	DSK6713_AIC23_INPUT_MIC; 			// select input
Uint32 fs 			= 	DSK6713_AIC23_FREQ_8KHZ; 			//set sampling rate


// include test routines
#include <struct.h>
#include <queue.h>
#include <init.h>
#include <dip_led.h>
// #include <out_sin_int.h>
#include <echo.h>

#endif /* MAIN_H_ */
