/*
 * maths.h
 *
 *  Created on: 14-Mar-2013
 *   Completed: 14-Mar-2013
 *      Author: Subhajit
 */

#ifndef MATHS_H_
#define MATHS_H_

#include <math.h>

#define maths_Pi       3.14159265358979323846

// Find Greatest Common Divisor of 2 numbers
int maths_GCD(int a, int b)
{
	if(b == 0) return a;
	return maths_GCD(b, a % b);
}

// Find Least Common Multiple of 2 numbers
#define	maths_LCM(a, b)		((a*b)/maths_GCD(a,b))

// find dot product of 2 floating-point arrays
float maths_dotProd(float* a, float* b, int length)
{
	int		i;
	float	sum = 0.0f;

	for(i=0; i<length; i++, a++, b++)
		sum += (*a) * (*b);
	return sum;
}

// find correlation between 2 floating point arrays
void maths_corr(float* dest, float* src_a, int a_len, float* src_b, int b_len)
{
	float	sum;
	int		i, j;
	float	*a, *b;

	for(i=0; i<a_len; i++)
	{
		sum = 0.0f;
		for(j=0, a=src_a+i, b=src_b; i<b_len; j++, a++, b++)
			sum += (*a) * (*b);
		*dest = sum;
		dest++;
	}
}

#endif /* MATHS_H_ */
