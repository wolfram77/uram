/*
 * mcbsp.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef MCBSP_H_
#define MCBSP_H_

/*--------------------------------------------------------------------------*/
/* TI Proprietary Information */
/* 08/03/01 */
/* mcbsp-init.c: */
/* */
/* McBSP0 is used in DLB mode with the (E)DMA to service the McBSP. */
/* CLKX and FSX generated using CPU clock and SRG. */
/*--------------------------------------------------------------------------*/

/* Chip definition, change this accordingly */
/* Include files */
#include <c6x.h>
#include <csl.h> /* CSL library */
#include <csl_dma.h> /* DMA_SUPPORT */
#include <csl_edma.h> /* EDMA_SUPPORT */
#include <csl_irq.h> /* IRQ_SUPPORT */
#include <csl_mcbsp.h> /* MCBSP_SUPPORT */
/*--------------------------------------------------------------------------*/

/* Define constants */
#define FALSE 0
#define TRUE 1
#define XFER_SIZE 128 /* Number of elements to transfer */
#define BUFFER_SIZE 256

/* Global variables used in interrupt ISRs */
volatile int recv0_done = FALSE;
volatile int xmit0_done = FALSE;
/*-------------------------------------------------------------------------------*/

/* Declare CSL objects */
MCBSP_Handle hMcbsp0; /* Handles for McBSP */
#if (DMA_SUPPORT)
	DMA_Handle hDma1; /* Handles for DMA */
	DMA_Handle hDma2;
#endif
#if (EDMA_SUPPORT) /* Handles for EDMA */
	EDMA_Handle hEdma1;
	EDMA_Handle hEdma2;
	EDMA_Handle hEdmadummy;
#endif
/*-------------------------------------------------------------------------------*/

/* External functions and function prototypes */
void init_mcbsp0(void);
void set_interrupts_dma(void);
void set_interrupts_edma(void);

/* Include the vector table to call the IRQ ISRs hookup */
extern far void vectors();

/*-------------------------------------------------------------------------------*/
/* main() */
/*-------------------------------------------------------------------------------*/
void main(void)
{
	/* Declaration of local variables */
	Uint32 xfer_size;
	Uint32 wait = 0;
	Uint32 y; /* Counter to initialize buffers */

	/* Create buffers. */
	#if (DMA_SUPPORT)
		static Uint32 dmaInbuff[BUFFER_SIZE]; /* buffer for DMA supporting devices */
		static Uint32 dmaOutbuff[BUFFER_SIZE];
	#endif
	#if (EDMA_SUPPORT)
		static Uint32 edmaInbuff[BUFFER_SIZE]; /* buffer for EDMA supporting devices */
		static Uint32 edmaOutbuff[BUFFER_SIZE];
	#endif
	IRQ_setVecs(vectors); /* point to the IRQ vector table */
	xfer_size = XFER_SIZE; /* set the size of the transfer */

	/* initialize the CSL library */
	CSL_init();

	/* initialize the the McBSP */
	init_mcbsp0();

	/* Enable sample rate generator - GRST=1 */
	MCBSP_enableSrgr(hMcbsp0);

	for (wait=0; wait<0x10; wait++); /* Wait states after SRG starts */

	#if (DMA_SUPPORT) /* for DMA supporting devices */
		DMA_reset(INV); /* reset all DMA channels */
	#endif
	
	#if (EDMA_SUPPORT) /* for EDMA supporting devices */
		EDMA_clearPram(0x00000000); /* Clear PaRAM RAM of the EDMA */
		set_interrupts_edma();
	#endif

	/*-------------------------------------------------------------------------------*/
	/* DMA channels 1 and 2 config structures */
	/*-------------------------------------------------------------------------------*/
	#if (DMA_SUPPORT) /* for DMA supporting devices */
		for (y=0;y<xfer_size;y++) {
		dmaOutbuff[y]=0x0000000+y; /* Initialize the transmit buffer */
		dmaInbuff[y]=0; /* Initialize the receive buffer */
		}

		/* Channel 2 transmits the data */
		hDma2 = DMA_open(DMA_CHA2, DMA_OPEN_RESET); /* Handle to DMA channel 2 */
		DMA_configArgs(hDma2,
		DMA_PRICTL_RMK(
		DMA_PRICTL_DSTRLD_DEFAULT,
		DMA_PRICTL_SRCRLD_DEFAULT,
		DMA_PRICTL_EMOD_DEFAULT,
		DMA_PRICTL_FS_DEFAULT,
		DMA_PRICTL_TCINT_ENABLE, /* TCINT =1 */
		DMA_PRICTL_PRI_DMA, /* DMA high priority */
		DMA_PRICTL_WSYNC_XEVT0, /* Set synchronization event XEVT0=01100 */
		DMA_PRICTL_RSYNC_DEFAULT,
		DMA_PRICTL_INDEX_DEFAULT,
		DMA_PRICTL_CNTRLD_DEFAULT,
		DMA_PRICTL_SPLIT_DEFAULT,
		DMA_PRICTL_ESIZE_32BIT, /* Element size 32 bits */
		DMA_PRICTL_DSTDIR_DEFAULT,
		DMA_PRICTL_SRCDIR_INC, /* Increment source by element size */
		DMA_PRICTL_START_DEFAULT
		),
		DMA_SECCTL_RMK(
		#if (!CHIP_6201)
			DMA_SECCTL_WSPOL_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
			DMA_SECCTL_RSPOL_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
			DMA_SECCTL_FSIG_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
		#endif
		DMA_SECCTL_DMACEN_DEFAULT,
		DMA_SECCTL_WSYNCCLR_DEFAULT,
		DMA_SECCTL_WSYNCSTAT_DEFAULT,
		DMA_SECCTL_RSYNCCLR_DEFAULT,
		DMA_SECCTL_RSYNCSTAT_DEFAULT,
		DMA_SECCTL_WDROPIE_DEFAULT,
		DMA_SECCTL_WDROPCOND_DEFAULT,
		DMA_SECCTL_RDROPIE_DEFAULT,
		DMA_SECCTL_RDROPCOND_DEFAULT,
		DMA_SECCTL_BLOCKIE_ENABLE, /* Enables DMA channel interrupt */
		DMA_SECCTL_BLOCKCOND_DEFAULT,
		DMA_SECCTL_LASTIE_DEFAULT,
		DMA_SECCTL_LASTCOND_DEFAULT,
		DMA_SECCTL_FRAMEIE_DEFAULT,
		DMA_SECCTL_FRAMECOND_DEFAULT,
		DMA_SECCTL_SXIE_DEFAULT,
		DMA_SECCTL_SXCOND_DEFAULT
		),
		DMA_SRC_RMK((Uint32)dmaOutbuff), /* Set source to dmaOutbuff */
		DMA_DST_RMK(MCBSP_ADDRH(hMcbsp0, DXR)), /* Set destination to DXR */
		DMA_XFRCNT_RMK(
		DMA_XFRCNT_FRMCNT_DEFAULT,
		DMA_XFRCNT_ELECNT_OF(xfer_size)
		)
		);

		/* Channel 1 receives the data */
		hDma1 = DMA_open(DMA_CHA1, DMA_OPEN_RESET); /* Handle to DMA channel 1 */
		DMA_configArgs(hDma1,
		DMA_PRICTL_RMK(
		DMA_PRICTL_DSTRLD_DEFAULT,
		DMA_PRICTL_SRCRLD_DEFAULT,
		DMA_PRICTL_EMOD_DEFAULT,
		DMA_PRICTL_FS_DEFAULT,
		DMA_PRICTL_TCINT_ENABLE, /* TCINT =1 */
		DMA_PRICTL_PRI_DMA, /* DMA high priority */
		DMA_PRICTL_WSYNC_DEFAULT,
		DMA_PRICTL_RSYNC_REVT0, /* Set synchronization event REVT0=01101 */
		DMA_PRICTL_INDEX_DEFAULT,
		DMA_PRICTL_CNTRLD_DEFAULT,
		DMA_PRICTL_SPLIT_DEFAULT,
		DMA_PRICTL_ESIZE_32BIT, /* Element size 32 bits */
		DMA_PRICTL_DSTDIR_INC, /* Increment destination by element size */
		DMA_PRICTL_SRCDIR_DEFAULT,
		DMA_PRICTL_START_DEFAULT
		),
		DMA_SECCTL_RMK(
		#if (!CHIP_6201)
			DMA_SECCTL_WSPOL_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
			DMA_SECCTL_RSPOL_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
			DMA_SECCTL_FSIG_NA, /* only on C6202(B)/C6203(B)/C6204/C6205 */
		#endif
		DMA_SECCTL_DMACEN_DEFAULT,
		DMA_SECCTL_WSYNCCLR_DEFAULT,
		DMA_SECCTL_WSYNCSTAT_DEFAULT,
		DMA_SECCTL_RSYNCCLR_DEFAULT,
		DMA_SECCTL_RSYNCSTAT_DEFAULT,
		DMA_SECCTL_WDROPIE_DEFAULT,
		DMA_SECCTL_WDROPCOND_DEFAULT,
		DMA_SECCTL_RDROPIE_DEFAULT,
		DMA_SECCTL_RDROPCOND_DEFAULT,
		DMA_SECCTL_BLOCKIE_ENABLE, /* Enables DMA channel interrupt */
		DMA_SECCTL_BLOCKCOND_DEFAULT,
		DMA_SECCTL_LASTIE_DEFAULT,
		DMA_SECCTL_LASTCOND_DEFAULT,
		DMA_SECCTL_FRAMEIE_DEFAULT,
		DMA_SECCTL_FRAMECOND_DEFAULT,
		DMA_SECCTL_SXIE_DEFAULT,
		DMA_SECCTL_SXCOND_DEFAULT
		),
		DMA_SRC_RMK(MCBSP_ADDRH(hMcbsp0, DRR)), /* Set source to DRR */
		DMA_DST_RMK((Uint32)dmaInbuff), /* Set destination to dmaInbuff */
		DMA_XFRCNT_RMK(
		DMA_XFRCNT_FRMCNT_DEFAULT,
		DMA_XFRCNT_ELECNT_OF(xfer_size)
		)
		);

		set_interrupts_dma(); /* initialize the interrupts */
		DMA_start(hDma1); /* Start DMA channels 1 and 2 */
		DMA_start(hDma2);
	#endif /* end for dma supporting devices */

	/*-------------------------------------------------------------------------------*/
	/* EDMA channels 12 and 13 config structures */
	/*-------------------------------------------------------------------------------*/
	#if (EDMA_SUPPORT) /* for EDMA supporting devices */
		for (y=0;y<xfer_size;y++) { /* Initialize the Outbuff */
		edmaOutbuff[y]=0x00000000+y;
		edmaInbuff[y]=0;
		}
		hEdma1 = EDMA_open(EDMA_CHA_REVT0, EDMA_OPEN_RESET);
		EDMA_configArgs(hEdma1,
		EDMA_OPT_RMK(
		EDMA_OPT_PRI_HIGH, /* High priority EDMA */
		EDMA_OPT_ESIZE_32BIT, /* Element size 32 bits */
		EDMA_OPT_2DS_NO,
		EDMA_OPT_SUM_NONE,
		EDMA_OPT_2DD_NO,
		EDMA_OPT_DUM_INC, /* Destination increment by element size */
		EDMA_OPT_TCINT_YES, /* Enable Transfer Complete Interrupt */
		EDMA_OPT_TCC_OF(13), /* TCCINT = 0xD, REVT0 */
		#if (C64_SUPPORT)
			EDMA_OPT_TCCM_DEFAULT,
			EDMA_OPT_ATCINT_DEFAULT,
			EDMA_OPT_ATCC_DEFAULT,
			EDMA_OPT_PDTS_DEFAULT,
			EDMA_OPT_PDTD_DEFAULT,
		#endif
		EDMA_OPT_LINK_YES, /* Enable linking to NULL table*/
		EDMA_OPT_FS_NO
		),
		EDMA_SRC_RMK(MCBSP_ADDRH(hMcbsp0, DRR)), /* Set source to DRR */
		EDMA_CNT_RMK(0,xfer_size),
		EDMA_DST_RMK((Uint32)edmaInbuff), /* Set destination to edmaInbuff */
		EDMA_IDX_RMK(0,0),
		EDMA_RLD_RMK(0,0)
		);
		hEdma2 = EDMA_open(EDMA_CHA_XEVT0, EDMA_OPEN_RESET);
		EDMA_configArgs(hEdma2,
		EDMA_OPT_RMK(
		EDMA_OPT_PRI_HIGH, /* High priority EDMA */
		EDMA_OPT_ESIZE_32BIT, /* Element size 32 bits */
		EDMA_OPT_2DS_NO,
		EDMA_OPT_SUM_INC, /* Source increment by element size */
		EDMA_OPT_2DD_NO,
		EDMA_OPT_DUM_NONE,
		EDMA_OPT_TCINT_YES, /* Enable Transfer Complete Interrupt */
		EDMA_OPT_TCC_OF(12), /* TCCINT = 0xC, XEVT0 */
		#if (C64_SUPPORT)
			EDMA_OPT_TCCM_DEFAULT,
			EDMA_OPT_ATCINT_DEFAULT,
			EDMA_OPT_ATCC_DEFAULT,
			EDMA_OPT_PDTS_DEFAULT,
			EDMA_OPT_PDTD_DEFAULT,
		#endif
		EDMA_OPT_LINK_YES, /* Enable linking to NULL table*/
		EDMA_OPT_FS_NO
		),
		EDMA_SRC_RMK((Uint32)edmaOutbuff), /* Set source to edmaOutbuff */
		EDMA_CNT_RMK(0,xfer_size),
		EDMA_DST_RMK(MCBSP_ADDRH(hMcbsp0, DXR)), /* Set destination to DXR0 */
		EDMA_IDX_RMK(0,0),
		EDMA_RLD_RMK(0,0)
		);
		hEdmadummy = EDMA_allocTable(-1); /* Dynamically allocates PaRAM RAM table */
		EDMA_configArgs(hEdmadummy, /* Dummy or Terminating Table in PaRAM */
		0x00000000, /* Terminate EDMA transfers by linking to */
		0x00000000, /* this NULL table */
		0x00000000,
		0x00000000,
		0x00000000,
		0x00000000
		);
		EDMA_link(hEdma1, hEdmadummy); /* Link terminating event to the EDMA event */
		EDMA_link(hEdma2, hEdmadummy);
		EDMA_enableChannel(hEdma1); /* Enable EDMA channels */
		EDMA_enableChannel(hEdma2);
	#endif /* End for EDMA supporting devices */
	MCBSP_enableRcv(hMcbsp0); /* Enable McBSP port 0 as the receiver */
	MCBSP_enableXmt(hMcbsp0); /* Enable McBSP port 0 as the transmitter */
	MCBSP_enableFsync(hMcbsp0);

	/* Wait for interrupt to the CPU when DMA transfer/receive is done */
	while (!xmit0_done || !recv0_done);
	MCBSP_close(hMcbsp0); /* close McBSP port */
	#if (DMA_SUPPORT) /* close DMA channels */
		DMA_close(hDma1);
		DMA_close(hDma2);
	#endif
	#if (EDMA_SUPPPORT)
		EDMA_close(hEdma1); /* close EDMA channels */
		EDMA_close(hEdma2);
		EDMA_freeTable(hEdmadummy);
	#endif
} /* end main */

/*-------------------------------------------------------------------------------*/
/* init_mcbsp0() */
/*-------------------------------------------------------------------------------*/
/* MCBSP Config structure */
/* Setup the MCBSP_0 as a master */
void init_mcbsp0(void)
{
	MCBSP_Config mcbspCfg0 = {
	#if (EDMA_SUPPORT)
		MCBSP_SPCR_RMK(
		MCBSP_SPCR_FREE_DEFAULT,
		MCBSP_SPCR_SOFT_DEFAULT,
		MCBSP_SPCR_FRST_YES, /* Frame sync not reset */
		MCBSP_SPCR_GRST_DEFAULT,
		MCBSP_SPCR_XINTM_XRDY, /* XINT driven by XRDY */
		MCBSP_SPCR_XSYNCERR_DEFAULT,
		MCBSP_SPCR_XRST_DEFAULT,
		MCBSP_SPCR_DLB_ON, /* Digital Loopback Mode enabled */
		MCBSP_SPCR_RJUST_RZF, /* Right-justify and zero-fill MSBs in DRR */
		MCBSP_SPCR_CLKSTP_DEFAULT,
		MCBSP_SPCR_DXENA_OFF,
		MCBSP_SPCR_RINTM_RRDY, /* RINT driven by RRDY */
		MCBSP_SPCR_RSYNCERR_DEFAULT,
		MCBSP_SPCR_RRST_DEFAULT
		),
	#endif
	#if (DMA_SUPPORT)
		MCBSP_SPCR_RMK(
		MCBSP_SPCR_FRST_NO,
		MCBSP_SPCR_GRST_DEFAULT,
		MCBSP_SPCR_XINTM_XRDY, /* XINT driven by XRDY */
		MCBSP_SPCR_XSYNCERR_DEFAULT,
		MCBSP_SPCR_XRST_DEFAULT,
		MCBSP_SPCR_DLB_ON, /* Digital Loopback Mode enabled */
		MCBSP_SPCR_RJUST_RZF, /* Right-justify and zero-fill MSBs in DRR */
		MCBSP_SPCR_CLKSTP_DEFAULT,
		MCBSP_SPCR_RINTM_RRDY, /* RINT driven by RRDY */
		MCBSP_SPCR_RSYNCERR_DEFAULT,
		MCBSP_SPCR_RRST_DEFAULT
		),
	#endif
	#if (EDMA_SUPPORT)
		MCBSP_RCR_RMK(
		MCBSP_RCR_RPHASE_SINGLE, /* Single phase frame */
		MCBSP_RCR_RFRLEN2_DEFAULT,
		MCBSP_RCR_RWDLEN2_DEFAULT,
		MCBSP_RCR_RCOMPAND_MSB, /* No companding */
		MCBSP_RCR_RFIG_YES, /* Ignore unexpected sync pulses */
		MCBSP_RCR_RDATDLY_1BIT, /* 1-bit delay */
		MCBSP_RCR_RFRLEN1_OF(0), /* 1 word per phase */
		MCBSP_RCR_RWDLEN1_32BIT, /* 32-bit receive element length */
		MCBSP_RCR_RWDREVRS_DISABLE
		),
	#endif
	#if (DMA_SUPPORT)
		MCBSP_RCR_RMK(
		MCBSP_RCR_RPHASE_SINGLE, /* Single phase frame */
		MCBSP_RCR_RFRLEN2_DEFAULT,
		MCBSP_RCR_RWDLEN2_DEFAULT,
		MCBSP_RCR_RCOMPAND_MSB, /* No companding */
		MCBSP_RCR_RFIG_YES, /* Ignore unexpected sync pulses */
		MCBSP_RCR_RDATDLY_1BIT, /* 1-bit delay */
		MCBSP_RCR_RFRLEN1_OF(0), /* 1 word per phase */
		MCBSP_RCR_RWDLEN1_32BIT /* 32-bit receive element length */
		),
	#endif
	#if (EDMA_SUPPORT)
		MCBSP_XCR_RMK(
		MCBSP_XCR_XPHASE_SINGLE, /* Single phase frame */
		MCBSP_XCR_XFRLEN2_DEFAULT,
		MCBSP_XCR_XWDLEN2_DEFAULT,
		MCBSP_XCR_XCOMPAND_MSB, /* No companding */
		MCBSP_XCR_XFIG_YES, /* Ignore unexpected sync pulses */
		MCBSP_XCR_XDATDLY_1BIT, /* 1-bit delay */
		MCBSP_XCR_XFRLEN1_OF(0), /* 1 word per phase */
		MCBSP_XCR_XWDLEN1_32BIT, /* 32-bit receive element length */
		MCBSP_XCR_XWDREVRS_DISABLE
		),
	#endif
	#if (DMA_SUPPORT)
		MCBSP_XCR_RMK(
		MCBSP_XCR_XPHASE_SINGLE, /* Single phase frame */
		MCBSP_XCR_XFRLEN2_DEFAULT,
		MCBSP_XCR_XWDLEN2_DEFAULT,
		MCBSP_XCR_XCOMPAND_MSB, /* No companding */
		MCBSP_XCR_XFIG_YES, /* Ignore unexpected sync pulses */
		MCBSP_XCR_XDATDLY_1BIT, /* 1-bit delay */
		MCBSP_XCR_XFRLEN1_OF(0), /* 1 word per phase */
		MCBSP_XCR_XWDLEN1_32BIT /* 32-bit receive element length */
		),
	#endif
	MCBSP_SRGR_RMK(
	MCBSP_SRGR_GSYNC_DEFAULT,
	MCBSP_SRGR_CLKSP_DEFAULT,
	MCBSP_SRGR_CLKSM_INTERNAL, /* srg clock derived internally */
	MCBSP_SRGR_FSGM_DXR2XSR,
	MCBSP_SRGR_FPER_DEFAULT,
	MCBSP_SRGR_FWID_DEFAULT,
	MCBSP_SRGR_CLKGDV_OF(7)
	),
	MCBSP_MCR_DEFAULT,
	MCBSP_RCER_DEFAULT,
	MCBSP_XCER_DEFAULT,
	MCBSP_PCR_RMK(
	MCBSP_PCR_XIOEN_DEFAULT,
	MCBSP_PCR_RIOEN_DEFAULT,
	MCBSP_PCR_FSXM_INTERNAL, /* Internal frame sync signals used */
	MCBSP_PCR_FSRM_INTERNAL, /* Internal frame sync signals used */
	MCBSP_PCR_CLKXM_OUTPUT,
	MCBSP_PCR_CLKRM_OUTPUT,
	MCBSP_PCR_CLKSSTAT_DEFAULT,
	MCBSP_PCR_DXSTAT_DEFAULT,
	MCBSP_PCR_FSXP_ACTIVEHIGH,
	MCBSP_PCR_FSRP_ACTIVEHIGH,
	MCBSP_PCR_CLKXP_RISING,
	MCBSP_PCR_CLKRP_FALLING
	)
	};
	hMcbsp0 = MCBSP_open(MCBSP_DEV0, MCBSP_OPEN_RESET);
	MCBSP_config(hMcbsp0, &mcbspCfg0);
}

/*-------------------------------------------------------------------------------*/
/* set_interrupts_dma() */
/*-------------------------------------------------------------------------------*/
#if (DMA_SUPPORT)
void /* Set the interrupts */ set_interrupts_dma(void) /* if the device supports DMA */
{
	IRQ_nmiEnable();
	IRQ_globalEnable();
	IRQ_disable(IRQ_EVT_DMAINT2);
	IRQ_disable(IRQ_EVT_DMAINT1); /* INT11 and INT9 */
	IRQ_clear(IRQ_EVT_DMAINT2);
	IRQ_clear(IRQ_EVT_DMAINT1);
	IRQ_enable(IRQ_EVT_DMAINT2);
	IRQ_enable(IRQ_EVT_DMAINT1);
	return;
}
#endif

/*-------------------------------------------------------------------------------*/
/* set_interrupts_edma() */
/*-------------------------------------------------------------------------------*/
#if (EDMA_SUPPORT)
void /* Set the interrupts */ set_interrupts_edma(void) /* if the device supports EDMA */
{
	IRQ_nmiEnable();
	IRQ_globalEnable();
	IRQ_reset(IRQ_EVT_EDMAINT);
	IRQ_disable(IRQ_EVT_EDMAINT);
	EDMA_intDisable(12); /* ch 12 for McBSP transmit event XEVT0 */
	EDMA_intDisable(13); /* ch 13 for McBSP receive event REVT0 */
	IRQ_clear(IRQ_EVT_EDMAINT);
	EDMA_intClear(12);
	EDMA_intClear(13);
	IRQ_enable(IRQ_EVT_EDMAINT);
	EDMA_intEnable(12);
	EDMA_intEnable(13);
	return;
}
#endif

/*-------------------------------------------------------------------------------*/
/* DMA DATA TRANSFER COMPLETION ISRs */
/*-------------------------------------------------------------------------------*/
interrupt void /* vecs.asm hooks this up to IRQ 11 */ c_int11(void) /* DMA ch2 */
{
	#if (DMA_SUPPORT)
		xmit0_done = TRUE;
		return;
	#endif
}
interrupt void /* vecs.asm hooks this up to IRQ 09 */ c_int09(void) /* DMA ch1 */
{
	#if (DMA_SUPPORT)
		recv0_done = TRUE;
		return;
	#endif
}
interrupt void /* vecs.asm hooks this up to IRQ 08 */ c_int08(void) /* for the EDMA */
{
	#if (EDMA_SUPPORT)
		if (EDMA_intTest(12))
		{
		xmit0_done = TRUE;
		EDMA_intClear(12); /* clear CIPR bit so future interrupts can be recognized */
		}
		else if (EDMA_intTest(13))
		{
		recv0_done = TRUE;
		EDMA_intClear(13); /* clear CIPR bit so future interrupts can be recognized */
		}
	#endif
	return;
}

/*-----------------------End of mcbsp-init.c-----------------------------------*/

#endif /* MCBSP_H_ */
