/*
 * memory.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 14-Mar-2013
 *      Author: Subhajit
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdlib.h>

#define		mem_Alloc(size, type)			((type*)malloc(size*sizeof(type)))
#define		mem_Realloc(ptr, size, type)	((type*)realloc(ptr, size*sizeof(type)))
#define		mem_Free(ptr)					(free(ptr))
#define		mem_Copy(dest, src, size)		memcpy(dest, src, size)

#endif /* MEMORY_H_ */
