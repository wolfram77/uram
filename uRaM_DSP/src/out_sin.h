/*
 * out_sin.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef OUT_SIN_H_
#define OUT_SIN_H_



void OutputSine()
{
	int time = 0; //table index
	int out_gain = 32000; //gain factor
	int time_length = 16;
	int toggle_time = 12000;
	double angle;
	double sine;
	int out_val;

	printf("Running OutputSine\n");
	while(1) //infinite loop
	{
		angle = (time * 2.0 * 3.14) / time_length;
		sine = sin(angle);
		out_val = (int) (sine * out_gain);
		output_sample(out_val); //output every Ts (SW0 on)
		// printf("%d\n", out_val);
		if(time % toggle_time == 0)
		{
			DSK6713_LED_on(0);
			DSK6713_LED_toggle(1);
			DSK6713_LED_off(2);
			DSK6713_LED_off(3);
			time = 0;
		}
		time++;
	}
}


#endif /* OUT_SIN_H_ */
