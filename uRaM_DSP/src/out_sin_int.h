/*
 * out_sin.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef OUT_SIN_H_
#define OUT_SIN_H_

int time = 0; //table index
int out_gain = 32000; //gain factor
int time_length = 16;
int toggle_time = 12000;
double angle;
double sine;
int out_val;

interrupt void c_int11()
{
//	int i;

	output_sample((time & 1) << 15);
	time++;
	return;
/*
	for(i=0; i<960; i++)
	{
		angle = (time * 2.0 * 3.14) / time_length;
		sine = sin(angle);
		out_val = (int) (sine * out_gain);
		output_sample(out_val); //output every Ts (SW0 on)
		// printf("%d\n", out_val);
		if(time % toggle_time == 0)
		{
			DSK6713_LED_on(0);
			DSK6713_LED_toggle(1);
			DSK6713_LED_off(2);
			DSK6713_LED_off(3);
			time = 0;
		}
		time++;
*/
}

void OutputSine()
{
	printf("Running OutputSine\n");
	while(1) //infinite loop
	{
	}
}


#endif /* OUT_SIN_H_ */
