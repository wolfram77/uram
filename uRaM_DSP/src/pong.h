/*
 * dong.h
 *
 *  Created on: 10-Mar-2013
 *      Author: Subhajit
 */

#ifndef DONG_H_
#define DONG_H_

#include "struct.h"
#include "queue.h"


// Dong Module
// Distance of obstruction sensing module
#define		TING_BUFFER_SIZE		1024
class Ting
// global variables for ting
private:
char*	ConsoleFile;
HANDLE	ConsoleFileHandle;
int		ConsoleFilePtr;
int		InputBlockSize;
int		OutputBlockSize;
int		NumInputBlocks;
int		BufferSize;
char*	Buffer;

// Functions for ting
public:
// initialize ting before use
Ting(char* console_file)
{
	BufferSize = TING_BUFFER_SIZE;
	Buffer = (char*) malloc(BufferSize);
	ConsoleFile = console_file;
	ConsoleFilePtr = 0;
	OutputBlockSize = sizeof(PONG_OUT);
	InputBlockSize = 2 * sizeof(PONG_OUT);
	NumInputBlocks = BufferSize / InputBlockSize;
	// open the console output file
	// which contains data in hex form
	// seek to the file position which needs to be read (fresh data)
	ConsoleFileHandle = CreateFile(ConsoleFile, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
									NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_TEMPORARY |
									FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH, NULL);
	if(ConsoleFileHandle == INVALID_HANDLE_VALUE) ConsoleFileHandle = NULL;
}
// close ting before exit
~Ting()
{
	free(Buffer);
	Buffer = NULL;
	ConsoleFile = NULL;
	ConsoleFilePtr = 0;
	OutputBlockSize = 0;
	InputBlockSize = 0;
	NumInputBlocks = 0;
	if(ConsoleFileHandle != NULL)
	{
		CloseHandle(ConsoleFileHandle);
		ConsoleFileHandle = NULL;
	}
}

// read block(s) of data from the console output file
// to ting output buffer
// returns - no. of input blocks read
int Read(void)
{
	int			i, input_blocks_read;
	PONG_OUT*	data_block;

	// try to read data from file, and put it in a temporary buffer
	input_blocks_read = ReadBlockFromFile(Buffer, BufferSize, InputBlockSize, NumInputBlocks, ConsoleFileHandle);
	// update the file ptr to point to the upcoming fresh data location
	ConsoleFilePtr += input_blocks_read * InputBlockSize;
	// convert all data in the buffer to binary data
	Hex2Bin(Buffer, Buffer, input_blocks_read * InputBlockSize);
	// add all blocks of data, one by one to the output queue
	data_block = (PONG_OUT*) Buffer;
	for(i=0; i<input_blocks_read; i++, data_block++)
	{
		TingOutQueue.push(*data_block);
	}
	return input_blocks_read;
}

private:
// reads blocks of data from the file
int ReadBlockFromFile(void* buffer, int buffer_size, int block_size, int num_blocks, HANDLE hFile)
{
	DWORD	read_bytes, file_ptr, new_ptr;
	int		blocks_read;

	// get the current file pointer
	file_ptr = SetFilePointer(hFile, 0, NULL, FILE_CURRENT);
	// read the desired no. of bytes
	ReadFile(hFile, buffer, num_blocks * block_size, &read_bytes, NULL);
	// calculate the no. of blocks read
	blocks_read = ((int) read_bytes) / block_size;
	// update the file pointer according to the number of blocks read
	new_ptr = file_ptr + (DWORD) (blocks_read * block_size);
	file_ptr += read_bytes;
	if(file_ptr != new_ptr) SetFilePointer(hFile, new_ptr, NULL, FILE_BEGIN);
	// return the no. of blocks read
	return blocks_read;
}
// convert a hex data stream to binary data stream
void Hex2Bin(void* hex_src, void* bin_dest, int src_size)
{
	char *src_hex, *dest_bin;
	char bin_val;
	int i, num_bytes;

	// get info in required form
	src_hex = (char*) hex_src;
	dest_bin = (char*) bin_dest;
	num_bytes = src_size / 2;
	// loop through all bytes to convert hex data to binary
	for(i=0; i<num_bytes; i++, src_hex+=2, dest_bin++)
	{
		bin_val = (src_hex[0] >= '0' && src_hex[0] <= '9')? (src_hex[0] - '0') : (src_hex[0] - 'A' + TOCHAR(10));
		bin_val <<= 4;
		bin_val |= (src_hex[1] >= '0' && src_hex[1] <= '9')? (src_hex[1] - '0') : (src_hex[1] - 'A' + TOCHAR(10));
		dest_bin[0] = bin_val;
	}
}
// convert a binary data stream to hex data stream
void Bin2Hex(void* bin_src, void* hex_dest, int src_size)
{
	char *src_bin, *dest_hex;
	char hex_val;
	int i, num_bytes;

	// get info in required form
	src_bin = (char*) bin_src;
	dest_hex = (char*) hex_dest;
	num_bytes = src_size;
	// loop through all bytes to convert hex data to binary
	for(i=0; i<num_bytes; i++, src_bin++, dest_hex+=2)
	{
		hex_val = src_bin[0] >> 4;
		hex_val = (hex_val <= TOCHAR(9))? (hex_val + '0') : (hex_val + 'A');
		dest_hex[0] = hex_val;
		hex_val = src_bin[0] & TOCHAR(0xF);
		hex_val = (hex_val <= TOCHAR(9))? (hex_val + '0') : (hex_val + 'A');
		dest_hex[1] = hex_val;
	}
}
};
void TingTest()
{
PONG_OUT	PongOut;
int			i, blocks_read;

Ting *dsk_ting = new Ting("out\\console.txt");
printf("Ting initialized\n");
// continuosly read and display
while(1)
{
	blocks_read = dsk_ting->Read();
	for(i=0; i<blocks_read; i++)
	{
		PongOut = TingOutQueue.front();
		TingOutQueue.pop();
		cout<<scientific<<"--\nPx = "<<PongOut.Px<<"\nPy = "<<PongOut.Py<<"\nPz = "<<PongOut.Pz<<"\n";
	}
}
delete dsk_ting;
}


#endif /* TING_H_ */
