/*
 * queue.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 15-Mar-2013
 *      Author: Subhajit
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdlib.h>
#include "memory.h"
#include "maths.h"
#include "semaphore.h"

// A grape bunch queue
typedef struct _QUEUE
{
	char*	Data;		// size at rear and front may be different
	int		Size;		// allocated size (bytes)
	int		MinSize;	// LCM of front and rear block size
	int		IncRate;	// factor of size increment
	int		DecRate;	// factor of size decrement
	int		Count;		// no. of bytes
	int		Rear;		// to the next byte
	int		Front;		// to the last byte
	int		Static;		// fixed size(1) or expandable(0)
	int		Mutex;		// busy(<=0) or free(>0)
}QUEUE;

// initialize
int queue_Init(QUEUE* queue, int rear_block_size, int front_block_size, int suggested_queue_size)
{
	int		block_sz;

	// a queue can be accessed only one at a time
	queue->Mutex = 1;
	semaphore_Wait(queue->Mutex)
	block_sz = maths_LCM(rear_block_size, front_block_size);
	queue->MinSize = block_sz;
	queue->Size = ((int)ceil(suggested_queue_size / (float)block_sz)) * block_sz;
	queue->Data = mem_Alloc(queue->Size, char);
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	// multiplying factor of minsize at which the size of queue increases / decreases
	queue->IncRate = 4;
	queue->DecRate = 4;
	// by default, queue does not resize automatically
	queue->Static = 1;
	semaphore_Signal(queue->Mutex)
	return 0;
}

// clean up all contents
int queue_Clear(QUEUE *queue)
{
	semaphore_Wait(queue->Mutex)
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	semaphore_Signal(queue->Mutex)
	return 0;
}

// free a queue
int queue_Close(QUEUE* queue)
{
	semaphore_Wait(queue->Mutex)
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	queue->Size = 0;
	mem_Free(queue->Data);
	semaphore_Signal(queue->Mutex)
	return 0;
}

// resize to a new desired size
int queue_Resize(QUEUE* queue, int new_size)
{
	int		new_front, data_count;

	semaphore_Wait(queue->Mutex)
	if(new_size == queue->Size || new_size < queue->Size)
	{
		semaphore_Signal(queue->Mutex)
		return -1;
	}
	new_size = ((int)ceil(new_size / (float)queue->MinSize)) * queue->MinSize;
	if(new_size > queue->Size)
	{
		queue->Data = mem_Realloc(queue->Data, new_size, char);
		if(queue->Count > 0)
		{
			if(queue->Rear <= queue->Front && queue->Front != 0)
			{
				new_front = queue->Front + new_size - queue->Size;
				data_count = queue->Size - queue->Front;
				mem_Copy(queue->Data + new_front, queue->Data + queue->Front, data_count);
				queue->Front = new_front;
			}
			else queue->Rear = queue->Size;
		}
	}
	else
	{
		if(queue->Count < new_size)
		{
			semaphore_Signal(queue->Mutex)
			return -1;
		}
		if(queue->Rear < queue->Front)
		{
			new_front = queue->Front - (queue->Size - new_size);
			data_count = queue->Size - queue->Front;
			mem_Copy(queue->Data + new_front, queue->Data + queue->Front, data_count);
			queue->Front = new_front;
		}
		else if(queue->Front > 0)
		{
			mem_Copy(queue->Data, queue->Data + queue->Front, queue->Count);
			queue->Front = 0;
			queue->Rear = queue->Count % queue->Size;
		}
		queue->Data = mem_Realloc(queue->Data, new_size, char);
	}
	queue->Size = new_size;
	semaphore_Signal(queue->Mutex)
	return 0;
}

// push data to queue
int queue_Push(QUEUE* queue, int block_sz, void *src)
{
	int		ret, templen;

	semaphore_Wait(queue->Mutex)
	// check for any expansion possibility
	if(queue->Count + block_sz > queue->Size)
	{
		if(queue->Static)
		{
			semaphore_Signal(queue->Mutex)
			return -1;
		}
		ret = queue_Resize(queue, queue->Size + queue->IncRate * queue->MinSize);
		if(ret == -1)
		{
			semaphore_Signal(queue->Mutex)
			return -1;
		}
	}
	if(src != NULL)
	{
		// push in one go, or in two gos
		if(queue->Rear + block_sz <= queue->Size) mem_Copy(queue->Data + queue->Rear, src, block_sz);
		else
		{
			templen = queue->Size - queue->Rear;
			mem_Copy(queue->Data + queue->Rear, src, templen);
			mem_Copy(queue->Data, ((char*)src) + templen, block_sz - templen);
		}
		queue->Count += block_sz;
		queue->Rear = ((queue->Rear + block_sz) % queue->Size);
	}
	semaphore_Signal(queue->Mutex)
	return 0;
}

// begin access a queue (for a continued set of queue operations)
int queue_AccessBegin(QUEUE* queue)
{
	semaphore_Wait(queue->Mutex);
	return 0;
}

// end access a queue
int queue_AccessEnd(QUEUE* queue)
{
	semaphore_Signal(queue->Mutex)
	return 0;
}

// check if queue has sufficient space for pushing
int queue_PushAvail(QUEUE* queue, int block_sz)
{
	semaphore_Wait(queue->Mutex)
	if(queue->Static == 0)
	{
		semaphore_Signal(queue->Mutex)
		return 0;
	}
	if((queue->Count + block_sz) > queue->Size)
	{
		semaphore_Signal(queue->Mutex)
		return -1;
	}
	semaphore_Signal(queue->Mutex)
	return 0;
}

// get data from the front of queue
int queue_Front(QUEUE* queue, int block_sz, void* dest)
{
	int		templen;

	semaphore_Wait(queue->Mutex)
	if(queue->Count < block_sz)
	{
		semaphore_Signal(queue->Mutex)
		return -1;
	}
	if(dest != NULL)
	{
		// front in one go, or in two gos
		if(queue->Front + block_sz <= queue->Size) mem_Copy(dest, queue->Data + queue->Front, block_sz);
		else
		{
			templen = queue->Size - queue->Front;
			mem_Copy(dest, queue->Data + queue->Front, templen);
			mem_Copy(((char*)dest) + templen, queue->Data, block_sz - templen);
		}
	}
	semaphore_Signal(queue->Mutex);
	return 0;
}

// pop data from queue
int queue_Pop(QUEUE* queue, int block_sz, void* dest)
{
	int		templen;

	semaphore_Wait(queue->Mutex)
	if(queue->Count < block_sz)
	{
		semaphore_Signal(queue->Mutex)
		return -1;
	}
	if(dest != NULL)
	{
		// pop in one go, or in two gos
		if(queue->Front + block_sz <= queue->Size) mem_Copy(dest, queue->Data + queue->Front, block_sz);
		else
		{
			templen = queue->Size - queue->Front;
			mem_Copy(dest, queue->Data + queue->Front, templen);
			mem_Copy(((char*)dest) + templen, queue->Data, block_sz - templen);
		}
		queue->Count -= block_sz;
		queue->Front = ((queue->Front + block_sz) % queue->Size);
		// check for any compression possibility
		if(queue->Static)
		{
			semaphore_Signal(queue->Mutex)
			return 0;
		}
		queue_Resize(queue, queue->Size - queue->DecRate * queue->MinSize);
	}
	semaphore_Signal(queue->Mutex)
	return 0;
}


#endif /* QUEUE_H_ */
