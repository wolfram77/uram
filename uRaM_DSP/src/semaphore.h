/*
 * semaphore.h
 *
 *  Created on: 14-Mar-2013
 *   Completed: 14-Mar_2013
 *      Author: Subhajit
 */

#ifndef SEMAPHORE_H_
#define SEMAPHORE_H_

#include "init.h"
#include "interrupt.h"

#ifdef DSP_PROCESSOR
	#define		semaphore_Wait(sem) \
	{\
	interrupt_Disable;\
	if((--sem)>0)interrupt_Enable;\
	}
	#define		semaphore_Signal(sem) \
	{\
	interrupt_Disable;\
	if((++sem)>0)interrupt_Enable;\
	}
#else
	#define		semaphore_Wait(sem)			{sem--;}
	#define		semaphore_Signal(sem)		{sem++;}
#endif

#endif /* SEMAPHORE_H_ */
