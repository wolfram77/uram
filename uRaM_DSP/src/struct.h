/*
 * struct.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef STRUCT_H_
#define STRUCT_H_


// global structure used in modules
typedef struct _DANG_OUT
{
	float	Tv;
	float	Dx;
	float	Dy;
	float	Dz;
	float	Ax;
	float	Ay;
	float	Az;
}DANG_OUT;

typedef struct _DONG_OUT
{
	float	Tv;
	float	Do;
}DONG_OUT;

typedef struct _PONG_OUT
{
	float	Px;
	float	Py;
	float	Pz;
	float	Cr;
	float	Cg;
	float	Cb;
}PONG_OUT, PLNG_POINT;

typedef struct _PLNG_OUT
{
	PLNG_POINT	P1;
	PLNG_POINT	P2;
	PLNG_POINT	P3;
}PLNG_OUT;


#endif /* STRUCT_H_ */
