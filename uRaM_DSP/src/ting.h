/*
 * ting.h
 *
 *  Created on: 06-Mar-2013
 *      Author: Subhajit
 */

#ifndef TING_H_
#define TING_H_

#include <Windows.h>
#include "struct.h"
#include "queue.h"


// Ting Module
// Transmission of information
#define		TING_BUFFER_SIZE		1024
// global variables for ting
QUEUE		ting_OutQueue;
char*		ting_ConsoleFile;
HANDLE		ting_ConsoleFileHandle;
int			ting_ConsoleFilePtr;
int			ting_InputBlockSize;
int			ting_OutputBlockSize;
int			ting_NumInputBlocks;
int			ting_BufferSize;
char*		ting_Buffer;

// Functions for ting
// initialize ting before use
void ting_Init(TING_OUT_QUEUE* ting_out_queue, char* console_file)
{
	ting_OutQueue = ting_out_queue;
	ting_BufferSize = TING_BUFFER_SIZE;
	ting_Buffer = (char*) malloc(ting_BufferSize);
	ting_ConsoleFile = console_file;
	ting_ConsoleFilePtr = 0;
	ting_OutputBlockSize = sizeof(PONG_OUT);
	ting_InputBlockSize = 2 * sizeof(PONG_OUT);
	ting_NumInputBlocks = ting_BufferSize / ting_InputBlockSize;
	// open the console output file
	// which contains data in hex form
	// seek to the file position which needs to be read (fresh data)
	ting_ConsoleFileHandle = CreateFile(ting_ConsoleFile, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
									NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_TEMPORARY |
									FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH, NULL);
	if(ting_ConsoleFileHandle == INVALID_HANDLE_VALUE) ting_ConsoleFileHandle = NULL;
}
// close ting before exit
void ting_Close()
{
	free(ting_Buffer);
	ting_Buffer = NULL;
	ting_ConsoleFile = NULL;
	ting_ConsoleFilePtr = 0;
	ting_OutputBlockSize = 0;
	ting_InputBlockSize = 0;
	ting_NumInputBlocks = 0;
	if(ting_ConsoleFileHandle != NULL)
	{
		CloseHandle(ting_ConsoleFileHandle);
		ting_ConsoleFileHandle = NULL;
	}
}

// read block(s) of data from the console output file
// to ting output buffer
// returns - no. of input blocks read
int ting_Read(void)
{
	int			i, input_blocks_read;
	PONG_OUT*	data_block;

	// try to read data from file, and put it in a temporary buffer
	input_blocks_read = ReadBlockFromFile(ting_Buffer, ting_BufferSize, ting_InputBlockSize, ting_NumInputBlocks, ting_ConsoleFileHandle);
	// update the file ptr to point to the upcoming fresh data location
	ting_ConsoleFilePtr += input_blocks_read * ting_InputBlockSize;
	// convert all data in the buffer to binary data
	Hex2Bin(ting_Buffer, ting_Buffer, input_blocks_read * ting_InputBlockSize);
	// add all blocks of data, one by one to the output queue
	data_block = (PONG_OUT*) ting_Buffer;
	for(i=0; i<input_blocks_read; i++, data_block++)
	{
		queue_push(ting_OutQueue, *data_block);
	}
	return input_blocks_read;
}

// reads blocks of data from the file
int ReadBlockFromFile(void* buffer, int buffer_size, int block_size, int num_blocks, HANDLE hFile)
{
	DWORD	read_bytes, file_ptr, new_ptr;
	int		blocks_read;

	// get the current file pointer
	file_ptr = SetFilePointer(hFile, 0, NULL, FILE_CURRENT);
	// read the desired no. of bytes
	ReadFile(hFile, buffer, num_blocks * block_size, &read_bytes, NULL);
	// calculate the no. of blocks read
	blocks_read = ((int) read_bytes) / block_size;
	// update the file pointer according to the number of blocks read
	new_ptr = file_ptr + (DWORD) (blocks_read * block_size);
	file_ptr += read_bytes;
	if(file_ptr != new_ptr) SetFilePointer(hFile, new_ptr, NULL, FILE_BEGIN);
	// return the no. of blocks read
	return blocks_read;
}

// convert a hex data stream to binary data stream
void Hex2Bin(void* hex_src, void* bin_dest, int src_size)
{
	char *src_hex, *dest_bin;
	char bin_val;
	int i, num_bytes;
	
	// get info in required form
	src_hex = (char*) hex_src;
	dest_bin = (char*) bin_dest;
	num_bytes = src_size / 2;
	// loop through all bytes to convert hex data to binary
	for(i=0; i<num_bytes; i++, src_hex+=2, dest_bin++)
	{
		bin_val = (src_hex[0] >= '0' && src_hex[0] <= '9')? (src_hex[0] - '0') : (src_hex[0] - 'A' + TOCHAR(10));
		bin_val <<= 4;
		bin_val |= (src_hex[1] >= '0' && src_hex[1] <= '9')? (src_hex[1] - '0') : (src_hex[1] - 'A' + TOCHAR(10));
		dest_bin[0] = bin_val;
	}
}

// convert a binary data stream to hex data stream
void Bin2Hex(void* bin_src, void* hex_dest, int src_size)
{
	char *src_bin, *dest_hex;
	char hex_val;
	int i, num_bytes;
	
	// get info in required form
	src_bin = (char*) bin_src;
	dest_hex = (char*) hex_dest;
	num_bytes = src_size;
	// loop through all bytes to convert hex data to binary
	for(i=0; i<num_bytes; i++, src_bin++, dest_hex+=2)
	{
		hex_val = src_bin[0] >> 4;
		hex_val = (hex_val <= TOCHAR(9))? (hex_val + '0') : (hex_val + 'A');
		dest_hex[0] = hex_val;
		hex_val = src_bin[0] & TOCHAR(0xF);
		hex_val = (hex_val <= TOCHAR(9))? (hex_val + '0') : (hex_val + 'A');
		dest_hex[1] = hex_val;
	}
}

void TingTest()
{
	PONG_OUT	PongOut;
	int			i, blocks_read;

	Ting *dsk_ting = new Ting("out\\console.txt");
	printf("Ting initialized\n");
	// continuosly read and display
	while(1)
	{
		blocks_read = dsk_ting->Read();
		for(i=0; i<blocks_read; i++)
		{
			PongOut = TingOutQueue.front();
			TingOutQueue.pop();
			cout<<scientific<<"--\nPx = "<<PongOut.Px<<"\nPy = "<<PongOut.Py<<"\nPz = "<<PongOut.Pz<<"\n";
		}
	}
	delete dsk_ting;
}


#endif /* TING_H_ */
