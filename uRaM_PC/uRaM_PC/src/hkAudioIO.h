/*
 * hkAudioIO.h
 *
 *  Created on: 15-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_AUDIO_IO_H_
#define HK_AUDIO_IO_H_

#include <hkVirtuoscope.h>

#ifdef	EXTERNAL_PROCESSOR
	#define		audTx_Init(disp_sz)
	#define		audRx_Init(disp_sz)
	#define		audTx(sample, amp)				output_sample((int)(sample * amp))
	#define		audRx(amp)						((float)input_sample() / amp)
	#define		audTx_Close()
	#define		audRx_Close()
#else
	VIRTUOSCOPE		audTxScope;
	VIRTUOSCOPE		audRxScope;
	#define			audTx_Init(disp_sz)			vsoInit(&audTxScope, "audTx.vso", sizeof(float), disp_sz)
	#define			audRx_Init(disp_sz)			vsoInit(&audRxScope, "audRx.vso", disp_sz, sizeof(float))
	#define			audTx(sample, amp)			vsoWrite(&audTxScope, (int)(sample * amp))
	#define			audRx(amp)					vsoWrite(&audTxScope, (int)(sample * amp))
	#define			audTx_Close()				vsoClose(&audTxScope)
	#define			audRx_Close()				vsoClose(&audRxScope)
#endif

#endif /* HK_AUDIO_IO_H_ */
