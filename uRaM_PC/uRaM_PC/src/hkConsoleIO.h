/*
 * hkConsoleIO.h
 *
 *  Created on: 01-Apr-2013
 *   Completed: 07-Apr_2013
 *      Author: Subhajit
 */

// #pragma once

#ifndef HK_CONSOLE_IO_H_
#define HK_CONSOLE_IO_H_

#include <stdio.h>
#include <string.h>

#ifdef	EXTERNAL_PROCESSOR
	#define	cnslWrite			printf
	#define	cnslRead			scanf
	#define	cnslWriteStr		puts
	#define	cnslReadStr			gets
#else
	#define	cnslWrite			printf
	#define	cnslRead			scanf
	#define	cnslWriteStr		puts
	#define	cnslReadStr			gets
#endif

#endif /* HK_CONSOLE_IO_H_ */
