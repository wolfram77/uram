/*
 * hkDataType.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_DATA_TYPE_H_
#define HK_DATA_TYPE_H_

#include <string.h>
#ifndef	EXTERNAL_PROCESSOR
#include <Windows.h>
typedef short			INT16, Int16;
typedef	unsigned short	UINT16, Uint16;
typedef	int				INT32, Int32;
typedef	unsigned int	UINT32, Uint32;
#endif
typedef	char			SBYTE, Sbyte;
typedef unsigned char	BYTE, Byte;

#define		TO_TYPE(x,typ)					((typ)x)
#define		TO_CHAR(x)						((char)x)
#define		TO_BYTE(x)						((BYTE)x)

// global structures used in modules
typedef struct _DANG_OUT
{
	float	Tv;
	float	Dx;
	float	Dy;
	float	Dz;
	float	Ax;
	float	Ay;
	float	Az;
}DANG_OUT;

typedef struct _DONG_OUT
{
	float	Tv;
	float	Do;
}DONG_OUT;

typedef struct _PONG_OUT
{
	float	Px;
	float	Py;
	float	Pz;
	float	Cr;
	float	Cg;
	float	Cb;
}PONG_OUT, PLNG_POINT;

typedef struct _PLNG_OUT
{
	PLNG_POINT	P1;
	PLNG_POINT	P2;
	PLNG_POINT	P3;
}PLNG_OUT;

#endif /* HK_DATA_TYPE_H_ */
