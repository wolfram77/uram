/*
 * hkDongModule.h
 *
 *  Created on: 10-Mar-2013
 *   Completed: 06-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_DONG_MODULE_H_
#define HK_DONG_MODULE_H_

#include <hkMath.h>
#include <hkFunnelQueue.h>
#include <hkInterrupt.h>
#include <hkAudioIO.h>

void dongInit();
void donginitSamplesPerSymbol();
void donginitSymbolsPerWord();
void donginitWordsPerFrame();
void donginitSymbols();

FUNNEL_QUEUE	dongTx;
FUNNEL_QUEUE	dongRx;
int		dongTxVal;
float	dongTxAmp			=	30000.0f;
float	dongRxAmp			=	30000.0f;
float	dongFs				=	96000.0f;
float	dongSymSfreq		=	40000.0f;
float	dongSym0freq		=	36000.0f;
float	dongSym1freq		=	44000.0f;
float*	dongSymS;
float*	dongSym0;
float*	dongSym1;
float	dongSymCycles		=	20.0f;
int		dongSymSamples;
int		dongWordSyms;
float	dongTframe			=	0.0066666667f;
int		dongFrameWords;
float	dongVsnd;
float*	dongRxBufInp;
float*	dongRxBufCorr;

// ISR for data transmission
intrOutputISR()
{
	float	sample = 0.0f;

	// play data in the transmission queue
	fqPop(&dongTx, sizeof(float), &sample);
	audTx(sample, dongTxAmp);
}

// ISR for data reception
intrInputISR()
{
	float	sample = 0.0f;

	// record data to reception queue
	sample = audRx(dongRxAmp);
	fqPush(&dongRx, sizeof(float), &sample);
}

// initialize
int dongInit(int suggested_tx_size, int suggested_rx_size)
{
	int		i, WordBytes;
	float	t, ts;
	float	tWord, minFreq, bits, count;

	// samples per symbol
	minFreq = (dongSymSfreq < dongSym0freq)? dongSymSfreq : dongSym0freq;
	minFreq = (dongSym1freq < minFreq)? dongSym1freq : minFreq;
	dongSymSamples = (int)ceil((dongSymCycles * dongFs) / minFreq);
	// symbols per word (excluding start symbol)
	bits = 0.0f;
	while(1)
	{
		count = (dongTframe * dongFs) / (dongSymSamples * (bits + 1));
		if(count <= pow(2, bits)) break;
		bits += 1.0f;
	}
	dongWordSyms = (int)bits;
	// words per frame
	tWord = dongSymSamples * (dongWordSyms + 1) / dongFs;
	dongFrameWords = (int)(dongTframe / tWord);
	// init symbols (start, 0, 1)
	dongSymS = memAlloc(dongSymSamples, float);
	dongSym0 = memAlloc(dongSymSamples, float);
	dongSym1 = memAlloc(dongSymSamples, float);
	ts = 1.0f / dongFs;
	for(i=0, t=0.0f; i<dongSymSamples; i++, t+=ts)
	{	dongSymS[i] = (float)sin(2.0*mathPi* dongSymSfreq * t);
		dongSym0[i] = (float)cos(2.0*mathPi* dongSym0freq * t);
		dongSym1[i] = (float)(-sin(2.0*mathPi* dongSym1freq * t));
	}
	// init Tx, Rx
	WordBytes = (dongWordSyms + 1) * dongSymSamples * sizeof(float);
	fqInit(&dongTx, WordBytes, sizeof(float), suggested_tx_size);
	fqInit(&dongRx, sizeof(float), WordBytes, suggested_rx_size);
	// Init Tx value
	dongTxVal = 0;
	// Init Rx buffers
	// dongRx_BufInp = memAlloc('
	return 0;
}

int dongTxWrite()
{
	float*	sym;
	int		i, val, ret, WordBytes, SymBytes;
	
	SymBytes = dongSymSamples * sizeof(float);
	WordBytes = (dongWordSyms + 1) * SymBytes;
	ret = fqPush(&dongTx, WordBytes, NULL);
	if(ret < 0) return -1;
	val = dongTxVal;
	fqPush(&dongTx, SymBytes, dongSymS);
	for(i=0; i<dongWordSyms; i++)
	{
		sym = ((val & 1) == 0)? dongSym0 : dongSym1;
		fqPush(&dongTx, SymBytes, sym);
	}
	dongTxVal = (dongTxVal + 1) % dongFrameWords;
	return 0;
}

int dongRxRead()
{
	int		ret, WordBytes, SymBytes;


	SymBytes = dongSymSamples * sizeof(float);
	WordBytes = (dongWordSyms + 1) * SymBytes;
	ret = fqFront(&dongRx, WordBytes<<1, dongRxBufInp);
	if(ret < 0) return -1;
	return 0;
}








#endif /* HK_DONG_MODULE_H_ */
