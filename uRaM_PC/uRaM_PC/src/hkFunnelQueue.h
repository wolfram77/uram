/* -------------------
 * | hkFunnelQueue.h |
 * -------------------
 *
 * FUNNEL_QUEUE module that implements a dynamic circular queue
 * with support for variable sized IO operations. Ideal
 * for use with any FIFO data structure requirement.
 *
 * FUNNEL_QUEUE module (this source file) is protable to a device
 * without requiring any modifications. However, its include
 * files may need appropriate implementation for the device
 * under use.
 *
 * FUNNEL_QUEUE module uses semaphore to block concurrent access
 * to any queue. "hkSemaphore.h" must be appropriately
 * defined for use with the desired device.
 *
 * FUNNEL_QUEUE module also uses "hkMemory.h" and "hkMath.h" which
 * may need modification depending upon the device being
 * used for development.
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit Sahu
 */

#pragma once

#ifndef HK_FUNNEL_QUEUE_H_
#define HK_FUNNEL_QUEUE_H_

#include <hkDataType.h>
#include <hkMemory.h>
#include <hkMath.h>
#include <hkSemaphore.h>

// A Funnel Queue
typedef struct _FUNNEL_QUEUE
{
	BYTE*		Data;		// size at rear and front may be different
	int			Size;		// allocated size (bytes)
	int			MinSize;	// LCM of front and rear block size
	int			IncRate;	// factor of size increment
	int			DecRate;	// factor of size decrement
	int			Count;		// no. of bytes
	int			Rear;		// to the next byte
	int			Front;		// to the last byte
	int			Static;		// fixed size(1) or expandable(0)
	SEMAPHORE	Mutex;		// busy(<=0) or free(>0)
}FUNNEL_QUEUE;

// initialize a queue (with a suggested queue size)
int fqInit(FUNNEL_QUEUE* queue, int rear_block_sz, int front_block_sz, int fqsz)
{
	int		block_sz;

	// a queue can be accessed only one at a time
	queue->Mutex = 1;
	semWait(queue->Mutex)
	block_sz = mathLCM(rear_block_sz, front_block_sz);
	queue->MinSize = block_sz;
	queue->Size = ((int)ceil(fqsz / (float)block_sz)) * block_sz;
	queue->Data = memAlloc(queue->Size, BYTE);
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	// multiplying factor of minsize at which the size of queue increases / decreases
	queue->IncRate = 4;
	queue->DecRate = 4;
	// by default, queue does not resize automatically
	queue->Static = 1;
	semSignal(queue->Mutex)
	return 0;
}

// clean up all contents
int fqClear(FUNNEL_QUEUE *queue)
{
	semWait(queue->Mutex)
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	semSignal(queue->Mutex)
	return 0;
}

// free a queue
int fqClose(FUNNEL_QUEUE* queue)
{
	semWait(queue->Mutex)
	queue->Count = 0;
	queue->Rear = 0;
	queue->Front = 0;
	queue->Size = 0;
	memFree(queue->Data);
	semSignal(queue->Mutex)
	return 0;
}

// resize to a new desired size
int fqResize(FUNNEL_QUEUE* queue, int new_size)
{
	int		new_front, data_count;

	semWait(queue->Mutex)
	new_size = ((int)ceil(new_size / (float)queue->MinSize)) * queue->MinSize;
	if(new_size == queue->Size)
	{
		semSignal(queue->Mutex)
		return -1;
	}
	else if(new_size > queue->Size)
	{
		queue->Data = memRealloc(queue->Data, new_size, BYTE);
		if(queue->Count > 0 && queue->Rear <= queue->Front)
		{
			if(queue->Front == 0) queue->Rear = queue->Size;
			else
			{
				new_front = queue->Front + new_size - queue->Size;
				data_count = queue->Size - queue->Front;
				// safely copy from source to destination
				memCopy(queue->Data + new_front, queue->Data + queue->Front, data_count);
				queue->Front = new_front;
			}
		}
	}
	else
	{
		if(queue->Count > new_size)
		{
			semSignal(queue->Mutex)
			return -1;
		}
		if(queue->Rear < queue->Front)
		{
			new_front = queue->Front - (queue->Size - new_size);
			data_count = queue->Size - queue->Front;
			memCopy(queue->Data + new_front, queue->Data + queue->Front, data_count);
			queue->Front = new_front;
		}
		else if(queue->Front > 0)
		{
			memCopy(queue->Data, queue->Data + queue->Front, queue->Count);
			queue->Front = 0;
			queue->Rear = queue->Count;
		}
		queue->Data = memRealloc(queue->Data, new_size, BYTE);
	}
	queue->Size = new_size;
	semSignal(queue->Mutex)
	return 0;
}

// check if queue has sufficient space for pushing
int fqPushAvail(FUNNEL_QUEUE* queue, int block_sz)
{
	if(queue->Static == 0) return 0;
	semWait(queue->Mutex)
	if((queue->Count + block_sz) > queue->Size)
	{
		semSignal(queue->Mutex)
		return -1;
	}
	semSignal(queue->Mutex)
	return 0;
}

// push data to queue (if src is not NULL)
int fqPush(FUNNEL_QUEUE* queue, int block_sz, void *src)
{
	int		ret, templen;

	semWait(queue->Mutex)
	// check for any expansion possibility
	if(queue->Count + block_sz > queue->Size)
	{
		if(queue->Static)
		{
			semSignal(queue->Mutex)
			return -1;
		}
		templen = (int)ceil((queue->Count + block_sz - queue->Size) / (float)(queue->IncRate * queue->MinSize));
		ret = fqResize(queue, queue->Size + templen * queue->IncRate * queue->MinSize);
		if(ret == -1)
		{
			semSignal(queue->Mutex)
			return -1;
		}
	}
	if(src != NULL)
	{
		// push in one go, or in two gos
		if(queue->Rear + block_sz <= queue->Size) memCopy(queue->Data + queue->Rear, src, block_sz);
		else
		{
			templen = queue->Size - queue->Rear;
			memCopy(queue->Data + queue->Rear, src, templen);
			memCopy(queue->Data, ((BYTE*)src) + templen, block_sz - templen);
		}
		queue->Count += block_sz;
		queue->Rear = ((queue->Rear + block_sz) % queue->Size);
	}
	semSignal(queue->Mutex)
	return 0;
}

// get data from the front of queue (if dest is not NULL)
int fqFront(FUNNEL_QUEUE* queue, int block_sz, void* dest)
{
	int		templen;

	semWait(queue->Mutex)
	if(queue->Count < block_sz)
	{
		semSignal(queue->Mutex)
		return -1;
	}
	if(dest != NULL)
	{
		// front in one go, or in two gos
		if(queue->Front + block_sz <= queue->Size) memCopy(dest, queue->Data + queue->Front, block_sz);
		else
		{
			templen = queue->Size - queue->Front;
			memCopy(dest, queue->Data + queue->Front, templen);
			memCopy(((BYTE*)dest) + templen, queue->Data, block_sz - templen);
		}
	}
	semSignal(queue->Mutex);
	return 0;
}

// pop data from queue (if dest is not NULL)
int fqPop(FUNNEL_QUEUE* queue, int block_sz, void* dest)
{
	int		templen;

	semWait(queue->Mutex)
	if(queue->Count < block_sz)
	{
		semSignal(queue->Mutex)
		return -1;
	}
	if(dest != NULL)
	{
		// pop in one go, or in two gos
		if(queue->Front + block_sz <= queue->Size) memCopy(dest, queue->Data + queue->Front, block_sz);
		else
		{
			templen = queue->Size - queue->Front;
			memCopy(dest, queue->Data + queue->Front, templen);
			memCopy(((BYTE*)dest) + templen, queue->Data, block_sz - templen);
		}
	}
	queue->Count -= block_sz;
	if(queue->Count > 0) queue->Front = ((queue->Front + block_sz) % queue->Size);
	else
	{
		queue->Front = 0;
		queue->Rear = 0;
	}
	// check for any compression possibility
	if(queue->Static)
	{
		semSignal(queue->Mutex)
		return 0;
	}
	templen = (queue->Size - queue->Count) / (queue->DecRate * queue->MinSize);
	templen = queue->Size - templen * queue->DecRate * queue->MinSize;
	if(templen >= queue->MinSize) fqResize(queue, templen);
	semSignal(queue->Mutex)
	return 0;
}

// begin access a queue (for a continued set of queue operations)
int fqAccessBegin(FUNNEL_QUEUE* queue)
{
	semWait(queue->Mutex);
	return 0;
}

// end access a queue
int fqAccessEnd(FUNNEL_QUEUE* queue)
{
	semSignal(queue->Mutex)
	return 0;
}

// provide pointer to the nth element in a queue
int fqGetElem(FUNNEL_QUEUE* queue, int block_sz, int elem_indx, void* elem)
{
	BYTE**		elem_ptr = (BYTE**)elem;

	elem_indx *= block_sz;
	if(elem_indx + block_sz > queue->Count) return -1;
	if(elem_ptr != NULL) *elem_ptr = queue->Data + ((queue->Front + elem_indx) % queue->Size);
	return 0;
}

// copy data from one queue to another (front from source queue)
int fqQcopy(FUNNEL_QUEUE* q_dest, FUNNEL_QUEUE* q_src, int block_sz)
{
	if(fqFront(q_src, block_sz, NULL) < 0 || fqPush(q_dest, block_sz, NULL) < 0) return -1;
	semWait(q_dest->Mutex)
	semWait(q_src->Mutex)
	memCopyCirc(q_dest->Data, q_dest->Size, q_dest->Rear, q_src->Data, q_src->Size, q_src->Front, block_sz);
	q_dest->Rear = (q_dest->Rear + block_sz) % q_dest->Size;
	q_dest->Count += block_sz;
	semSignal(q_src->Mutex)
	semSignal(q_dest->Mutex)
	return 0;
}

// move data from one queue to another (pop from source queue)
int fqQmove(FUNNEL_QUEUE* q_dest, FUNNEL_QUEUE* q_src, int block_sz)
{
	if(fqFront(q_src, block_sz, NULL) < 0 || fqPush(q_dest, block_sz, NULL) < 0) return -1;
	semWait(q_dest->Mutex)
	semWait(q_src->Mutex)
	memCopyCirc(q_dest->Data, q_dest->Size, q_dest->Rear, q_src->Data, q_src->Size, q_src->Front, block_sz);
	q_dest->Rear = (q_dest->Rear + block_sz) % q_dest->Size;
	q_dest->Count += block_sz;
	fqPop(q_src, block_sz, NULL);
	semSignal(q_src->Mutex)
	semSignal(q_dest->Mutex)
	return 0;
}


#endif /* HK_FUNNEL_QUEUE_H_ */
