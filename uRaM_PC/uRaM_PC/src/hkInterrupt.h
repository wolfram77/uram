/*
 * hkInterrupt.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr_2013
 *      Author: Subhajit
 */

#ifndef HK_INTERRUPT_H_
#define HK_INTERRUPT_H_

#ifdef	EXTERNAL_PROCESSOR
	#define		intrInputISR			interrupt void c_int11
	#define		intrOutputISR			interrupt void c_int11
#else
	#define		intrInputISR			void InputISR
	#define		intrOutputISR			void OutputISR
#endif

#endif /* HK_INTERRUPT_H_ */
