/*
 * hkMainModule.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#ifndef HK_MAIN_H_
#define HK_MAIN_H_

// include headers
#ifdef	EXTERNAL_PROCESSOR
#include <hkAudioIO.h>
#else
#include <hkVirtuoscope.h>
#endif
#include <hkConsoleIO.h>
#include <hkDataType.h>
#include <hkFunnelQueue.h>
#include <hkInterrupt.h>
#include <hkMath.h>
#include <hkMemory.h>
#include <hkSemaphore.h>

// include modules
#include <hkTingModule.h>
#include <hkDongModule.h>

// include initialization
#include <hkInitilization.h>

#endif /* HK_MAIN_H_ */
