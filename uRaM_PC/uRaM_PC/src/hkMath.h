/*
 * hkMaths.h
 *
 *  Created on: 14-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#ifndef HK_MATHS_H_
#define HK_MATHS_H_

#include <math.h>

#define mathPi       3.14159265358979323846

#define	mathMin(a, b)\
	((a <= b)? a : b)

#define	mathMin3(a, b, c)\
	(mathMin(mathMin(a, b), c))

#define	mathMinNat(a, b)\
	(((a <= b && a > 0) || (b <= 0))? a : b)
	
#define	mathMinNat3(a, b, c)\
	(mathMinNat(mathMinNat(a, b), c))
	
#define	mathMax(a, b)\
	((a >= b)? a : b)

#define	mathMax3(a, b, c)\
	(mathMax(mathMax(a, b), c))


// Find Greatest Common Divisor of 2 numbers
int mathGCD(int a, int b)
{
	if(b == 0) return a;
	return mathGCD(b, a % b);
}

// Find Least Common Multiple of 2 numbers
#define	mathLCM(a, b)\
	((a*b)/mathGCD(a,b))

// find dot product of 2 floating-point arrays
float mathDotProd(float* a, float* b, int length)
{
	float*	a_end;
	float	sum = 0.0f;

	sum = 0.0f;
	a_end = a + length;
	for(; a<a_end; a++, b++)
		sum += (*a) * (*b);
	return sum;
}

// find limited correlation between 2 floating point arrays
int mathLimCorr(float* dest, float* a, int a_sz, float* b, int b_sz)
{
	float	sum;
	int		i;
	float*	a_end;

	if(a_sz < b_sz) return -1;
	a_end = a + (a_sz - b_sz + 1);
	for(; a<a_end; a++, dest++)
	{
		sum = 0.0f;
		for(i=0; i<b_sz; i++)
			sum += a[i] * b[i];
		*dest = sum;
	}
	return 0;
}

#endif /* HK_MATHS_H_ */
