/*
 * hkMemory.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#ifndef HK_MEMORY_H_
#define HK_MEMORY_H_

#include <stdlib.h>
#include <hkMath.h>

#define		memAlloc(size, type)			((type*)malloc(size*sizeof(type)))
#define		memRealloc(ptr, size, type)	((type*)realloc(ptr, size*sizeof(type)))
#define		memFree(ptr)					(free(ptr))
#define		memCopy(dest, src, size)		memcpy(dest, src, size)

// Safely copies data from source to destination
int memCopySafe(void* dest, void* src, int size)
{
	char			*c_src, *c_dest;
	double			*d_src, *d_dest, *d_end;

	// no copying required
	if(src == dest) return -1;
	// forward copying required
	if(src > dest)
	{
		// copy 8 bytes at a time
		d_src = (double*)src;
		d_dest = (double*)dest;
		d_end = d_src + (size >> 3);
		for(; d_src<d_end; d_src++, d_dest++) *d_dest = *d_src;
		// copy remaining as 1 byte at a time
		c_src = (char*)d_src;
		c_dest = (char*)d_dest;
		src = (char*)src + size - 1;
		for(; c_src<=(char*)src; c_src++, c_dest++) *c_dest = *c_src;
	}
	// backward copying required
	else
	{
		// copy 8 bytes at a time
		d_src = (double*)((char*)src + size) - 1;
		d_dest = (double*)((char*)dest + size) - 1;
		for(; d_src>=(double*)src; d_src--, d_dest--) *d_dest = *d_src;
		// copy remaining as 1 byte at a time
		c_src = (char*)(d_src + 1) - 1;
		c_dest = (char*)(d_dest + 1) - 1;
		for(; c_src>=(char*)src; c_src--, c_dest--) *c_dest = *c_src;
	}
	return 0;
}

// Copy data from one circular memory to another circular memory (such as in queue)
int memCopyCirc(void* dest, int dest_sz, int dest_off, void* src, int src_sz, int src_off, int size)
{
	int		dest_d, src_d, min_d;

	if(mathMin(dest_sz, src_sz) < size) return -1;
	// --- copy 1st minimum block
	if(size <= 0) return 0;
	dest_off = dest_off % dest_sz;
	src_off = src_off % src_sz;
	dest_d = dest_sz - dest_off;
	src_d = src_sz - src_off;
	min_d = mathMin3(dest_d, src_d, size);
	memcpy((char*)dest + dest_off, (char*)src + src_off, min_d);
	dest_d -= min_d;
	src_d -= min_d;
	size -= min_d;
	// --- copy 2nd minimum block
	if(size <= 0) return 0;
	dest_off = (dest_off + min_d) % dest_sz;
	src_off = (src_off + min_d) % src_sz;
	min_d = mathMinNat3(dest_d, src_d, size);
	memcpy((char*)dest + dest_off, (char*)src + src_off, min_d);
	size -= min_d;
	// --- copy last block
	if(size <= 0) return 0;
	dest_off = (dest_off + min_d) % dest_sz;
	src_off = (src_off + min_d) % src_sz;
	memcpy((char*)dest + dest_off, (char*)src + src_off, size);
	return 0;
}

#endif /* HK_MEMORY_H_ */
