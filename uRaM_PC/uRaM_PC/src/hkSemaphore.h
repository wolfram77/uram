/*
 * hkSemaphore.h
 *
 *  Created on: 14-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_SEMAPHORE_H_
#define HK_SEMAPHORE_H_

#include <hkInterrupt.h>

#define	SEMAPHORE		int

// global variables
SEMAPHORE		_semWaitCount;

#ifdef EXTERNAL_PROCESSOR
	#define		semWait(sem) \
		{\
		if(_semWaitCount == 0) IRQ_globalDisable();\
		_semWaitCount += ((--sem) == 0)? 1 : 0;
		if(_semWaitCount == 0) IRQ_globalEnable();\
		}
	#define		semSignal(sem) \
		{\
		if(_semWaitCount == 0) IRQ_globalDisable();\
		_semWaitCount -= ((sem++) == 0)? 1 : 0;
		if(_semWaitCount == 0) IRQ_globalEnable();\
		}
#else
	#define		semWait(sem) \
		_semWaitCount += ((--sem) == 0)? 1 : 0;
	#define		semSignal(sem) \
		_semWaitCount -= ((sem++) == 0)? 1 : 0;
#endif

// Starts the SEMAPHORE module
int semStart()
{
	_semWaitCount = 0;
#ifdef	EXTERNAL_PROCESSOR
	IRQ_globalEnable();
	IRQ_nmiEnable();
#endif
	return 0;
}

// Stops the SEMAPHORE module
int semStop()
{
	return semStart();
}

#endif /* HK_SEMAPHORE_H_ */
