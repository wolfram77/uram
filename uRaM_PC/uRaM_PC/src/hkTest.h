/*
 * hkTest.h
 *
 *  Created on: 30-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_H_
#define HK_TEST_H_

#include <hkDataType.h>
#include <hkConsoleIO.h>
#include <stdlib.h>

// Get the function number called
int tstFuncNum(char** func_names, int func_count, char* func_name)
{
	int		i;

	for(i=0; i<func_count; i++)
	{
		if(_strcmpi(func_names[i], func_name) == 0) return i;
	}
	return -1;
}

// Display character array in char form
int tstDisplayChar(char* src_disp, char* src, int src_sz, char* sep_disp)
{
	int		i, src_disp_len, sep_disp_len, ch_disp, ch_count;

	ch_count = 0;
	src_disp_len = strlen(src_disp);
	sep_disp_len = strlen(sep_disp);
	ch_disp = (78 - src_disp_len) / (2 + sep_disp_len);
	// display src name
	cnslWrite("%s", src_disp);
	// display all characters as hex
	while(src_sz)
	{
		// display chararacter
		cnslWrite("%c%s", *src, sep_disp);
		src++;
		src_sz--;
		ch_count++;
		// when one line is complete ...
		if(ch_count == ch_disp)
		{
			cnslWrite("\n");
			for(i=0; i<src_disp_len; i++) cnslWrite(" ");
			ch_count = 0;
		}
	}
	if(ch_count || src_sz == 0) cnslWrite("\n");
	return 0;
}

// Display character array in hex form
int tstDisplayHex(char* src_disp, char* src, int src_sz, char* sep_disp)
{
	char	hex_hi, hex_lo;
	int		i, src_disp_len, sep_disp_len, ch_disp, ch_count;

	ch_count = 0;
	src_disp_len = strlen(src_disp);
	sep_disp_len = strlen(sep_disp);
	ch_disp = (78 - src_disp_len) / (2 + sep_disp_len);
	// display src name
	cnslWrite("%s", src_disp);
	// display all characters as hex
	while(src_sz)
	{
		// display hex of a chararacter
		hex_hi = (*src >> 4) & 0xF;
		hex_hi = (hex_hi >= TO_CHAR(10))? (hex_hi + 'A' - TO_CHAR(10)) : (hex_hi + '0');
		hex_lo = (*src & 0xF);
		hex_lo = (hex_lo >= TO_CHAR(10))? (hex_lo + 'A' - TO_CHAR(10)) : (hex_lo + '0');
		cnslWrite("%c%c%s", hex_hi, hex_lo, sep_disp);
		src++;
		src_sz--;
		ch_count++;
		// when one line is complete ...
		if(ch_count == ch_disp)
		{
			cnslWrite("\n");
			for(i=0; i<src_disp_len; i++) cnslWrite(" ");
			ch_count = 0;
		}
	}
	if(ch_count || src_sz == 0) cnslWrite("\n");
	return 0;
}

// Display integer array in integer(4) form
int tstDisplayInt(char* src_disp, int* src, int src_sz, char* sep_disp)
{
	int		i, src_disp_len, sep_disp_len, ch_disp, ch_count;

	ch_count = 0;
	src_disp_len = strlen(src_disp);
	sep_disp_len = strlen(sep_disp);
	ch_disp = (78 - src_disp_len) / (2 + sep_disp_len);
	// display src name
	cnslWrite("%s", src_disp);
	// display all characters as hex
	while(src_sz)
	{
		// display chararacter
		cnslWrite("%4d%s", *src, sep_disp);
		src++;
		src_sz--;
		ch_count++;
		// when one line is complete ...
		if(ch_count == ch_disp)
		{
			cnslWrite("\n");
			for(i=0; i<src_disp_len; i++) cnslWrite(" ");
			ch_count = 0;
		}
	}
	if(ch_count || src_sz == 0) cnslWrite("\n");
	return 0;
}

// Display float array in float(5) form
int tstDisplayFloat(char* src_disp, float* src, int src_sz, char* sep_disp)
{
	int		i, src_disp_len, sep_disp_len, ch_disp, ch_count;

	ch_count = 0;
	src_disp_len = strlen(src_disp);
	sep_disp_len = strlen(sep_disp);
	ch_disp = (78 - src_disp_len) / (7 + sep_disp_len);
	// display src name
	cnslWrite("%s", src_disp);
	// display all characters as hex
	while(src_sz)
	{
		// display chararacter
		cnslWrite("%5f%s", *src, sep_disp);
		src++;
		src_sz--;
		ch_count++;
		// when one line is complete ...
		if(ch_count == ch_disp)
		{
			cnslWrite("\n");
			for(i=0; i<src_disp_len; i++) cnslWrite(" ");
			ch_count = 0;
		}
	}
	if(ch_count || src_sz == 0) cnslWrite("\n");
	return 0;
}

#endif /* HK_TEST_H_ */
