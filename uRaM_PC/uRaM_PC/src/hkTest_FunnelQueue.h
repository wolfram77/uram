/*
 * hkTest_FunnelQueue.h
 *
 *  Created on: 28-Mar-2013
 *   Completed: 28-Mar-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_FUNNEL_QUEUE_H_
#define HK_TEST_FUNNEL_QUEUE_H_

#include <hkTest.h>
#include <hkFunnelQueue.h>

// global variables
FUNNEL_QUEUE	_fqTest_Queue;
FUNNEL_QUEUE	_fqTest_Queue2;
char	_fqTest_Buffer[1024];
char	_fqTest_DispBuffer[1024];
int		_fqTest_funcCount = 13;
char*	_fqTest_funcName[] = {"init", "clear", "close", "resize", "pushavail", "push", "front", "pop", "accessbegin", "accessend", "getelem", "qcopy", "qmove"};
char*	_fqTest_funcDesc[] = {
	"fqInit(FUNNEL_QUEUE* queue, int rear_block_sz, int front_block_sz, int fqsz)",
	"fqClear(FUNNEL_QUEUE *queue)",
	"fqClose(FUNNEL_QUEUE* queue)",
	"fqResize(FUNNEL_QUEUE* queue, int new_size)",
	"fqPushAvail(FUNNEL_QUEUE* queue, int block_sz)",
	"fqPush(FUNNEL_QUEUE* queue, int block_sz, void *src)",
	"fqFront(FUNNEL_QUEUE* queue, int block_sz, void* dest)",
	"fqPop(FUNNEL_QUEUE* queue, int block_sz, void* dest)",
	"fqAccessBegin(FUNNEL_QUEUE* queue)",
	"fqAccessEnd(FUNNEL_QUEUE* queue)",
	"fqGetElem(FUNNEL_QUEUE* queue, int block_sz, int elem_indx, void* elem)",
	"fqQcopy(FUNNEL_QUEUE* q_dest, FUNNEL_QUEUE* q_src, int block_sz)",
	"fqQmove(FUNNEL_QUEUE* q_dest, FUNNEL_QUEUE* q_src, int block_sz)"
};

// Display the status of queue
int _fqTest_Display(FUNNEL_QUEUE *q, int ret_val)
{
	cnslWrite("Queue status: %d ------\n", ret_val);
	cnslWrite("Front: %4d   Rear: %7d   Count: %4d   Size: %5d   Mutex: %2d\n", q->Front, q->Rear, q->Count, q->Size, q->Mutex);
	cnslWrite("Static: %3d   MinSize: %4d   IncRate: %2d   DecRate: %2d   \n", q->Static, q->MinSize, q->IncRate, q->DecRate);
	fqFront(q, q->Count, _fqTest_Buffer);
	tstDisplayChar("List: ", _fqTest_Buffer, q->Count, " ");
	tstDisplayChar("Data: ", (char*)q->Data, q->Size, " ");
	tstDisplayChar("Data: ", _fqTest_DispBuffer, q->Size, " ");
	fqFront(&_fqTest_Queue2, _fqTest_Queue2.Count, _fqTest_Buffer);
	tstDisplayChar("FQ2: ", _fqTest_Buffer, _fqTest_Queue2.Count, " ");
	cnslWrite("\n");
	return 0;
}

// Accept one command and display status (-2 = exit requested)
int _fqTest_DoOp(FUNNEL_QUEUE* q)
{
	int		ret, func;
	char	*cptr, func_name[256];
	int		i1, i2, i3;
	char	c1[256];

	cptr = NULL;
	cnslWrite("(Enter command) fq: ");
	cnslRead("%s", func_name);
	if(_strcmpi(func_name, "exit") == 0) return -2;
	func = tstFuncNum(_fqTest_funcName ,_fqTest_funcCount, func_name);
	if(func < 0)
	{
		cnslWrite("No such function.\n\n");
		return -1;
	}
	cnslWrite("%s\n", _fqTest_funcDesc[func]);
	cnslWrite("Parameters: ");
	switch(func)
	{
	case 0:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		ret = fqInit(q, i1, i2, i3);
		q->Static = 0;
		break;
	case 1:
		ret = fqClear(q);
		break;
	case 2:
		ret = fqClose(q);
		break;
	case 3:
		cnslRead("%d", &i1);
		ret = fqResize(q, i1);
		break;
	case 4:
		cnslRead("%d", &i1);
		ret = fqPush(q, i1, NULL);
		break;
	case 5:
		cnslRead("%d%s", &i1, c1);
		ret = fqPush(q, i1, c1);
		break;
	case 6:
		cnslRead("%d", &i1);
		ret = fqFront(q, i1, _fqTest_DispBuffer);
		break;
	case 7:
		cnslRead("%d", &i1);
		ret = fqPop(q, i1, _fqTest_DispBuffer);
		break;
	case 8:
		ret = fqAccessBegin(q);
		break;
	case 9:
		ret = fqAccessEnd(q);
		break;
	case 10:
		cnslRead("%d%d", &i1, &i2);
		ret = fqGetElem(q, i1, i2, &cptr);
		if(ret == 0) *_fqTest_DispBuffer = *cptr;
		break;
	case 11:
		cnslRead("%d", &i1);
		ret = fqQcopy(&_fqTest_Queue2, q, i1);
		break;
	case 12:
		cnslRead("%d", &i1);
		ret = fqQmove(&_fqTest_Queue2, q, i1);
	}
	_fqTest_Display(q, ret);
	return 0;
}

// Interactive queue test
void fqTest_Run()
{
	int		ret;

	cnslWrite("Welcome to queueTest\n");
	cnslWrite("--------------------\n\n");
	cnslWrite("This is a testbench for FUNNEL_QUEUE module\n");
	system("PAUSE");
	cnslWrite("\n\n");
	fqInit(&_fqTest_Queue2, 1, 1, 20);
	do
	{
		ret = _fqTest_DoOp(&_fqTest_Queue);
	}while(ret != -2);
}



#endif /* HK_TEST_FUNNEL_QUEUE_H_ */
