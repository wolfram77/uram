/*
 * hkTest_Math.h
 *
 *  Created on: 01-Apr-2013
 *   Completed: 01-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_MATH_H_
#define HK_TEST_MATH_H_

#include <hkTest.h>
#include <hkMath.h>

// global variables
#define	MATHTEST_DISPLAY_COUNT		80
float	_mathTest_Buffer[256];
int		_mathTest_funcCount = 11;
char*	_mathTest_funcName[] = {"write", "min", "min3", "minnatural", "minnatural3", "max", "max3", "gcd", "lcm", "dotprod", "limcorr"};
char*	_mathTest_funcDesc[] = {
	"Pseudo float write function(float, address)",
	"mathMin(a, b)",
	"mathMin3(a, b, c)",
	"mathMinNat(a, b)",
	"mathMinNat3(a, b, c)",
	"mathMax(a, b)",
	"mathMax3(a, b, c)",
	"mathGCD(int a, int b)",
	"mathLCM(a, b)",
	"mathDotProd(float* a, float* b, int length)",
	"mathLimCorr(float* dest, float* src_a, int a_len, float* src_b, int b_len)"
};

// Display the status of maths buffers
int _mathTest_Display(int ret_val, float out_val)
{
	cnslWrite("Maths status: %d ------\n", ret_val);
	cnslWrite("Output value: %f\n", out_val);
	tstDisplayFloat("Buffer:  ", _mathTest_Buffer, MATHTEST_DISPLAY_COUNT, "  ");
	cnslWrite("\n");
	return 0;
}

// Accept one command and display status (-2 = exit requested)
int _mathTest_DoOp()
{
	float	out;
	int		ret, func;
	char	func_name[256];
	float	f1, f2, f3;
	int		i1, i2, i3, i4, i5;

	ret = 0;
	out = 0.0f;
	cnslWrite("(Enter command) math: ");
	cnslRead("%s", func_name);
	if(_strcmpi(func_name, "exit") == 0) return -2;
	func = tstFuncNum(_mathTest_funcName, _mathTest_funcCount, func_name);
	if(func < 0)
	{
		cnslWrite("No such function.\n\n");
		return -1;
	}
	cnslWrite("%s\n", _mathTest_funcDesc[func]);
	cnslWrite("Parameters: ");
	switch(func)
	{
	case 0:
		cnslRead("%f%d", &f1, &i1);
		_mathTest_Buffer[i1] = f1;
		break;
	case 1:
		cnslRead("%f%f", &f1, &f2);
		out = mathMin(f1, f2);
		break;
	case 2:
		cnslRead("%f%f%f", &f1, &f2, &f3);
		out = mathMin3(f1, f2, f3);
		break;
	case 3:
		cnslRead("%f%f", &f1, &f2);
		out = mathMinNat(f1, f2);
		break;
	case 4:
		cnslRead("%f%f%f", &f1, &f2, &f3);
		out = mathMinNat3(f1, f2, f3);
		break;
	case 5:
		cnslRead("%f%f", &f1, &f2);
		out = mathMax(f1, f2);
		break;
	case 6:
		cnslRead("%f%f%f", &f1, &f2, &f3);
		out = mathMax3(f1, f2, f3);
		break;
	case 7:
		cnslRead("%d%d", &i1, &i2);
		out = (float)mathGCD(i1, i2);
		break;
	case 8:
		cnslRead("%d%d", &i1, &i2);
		out = (float)mathLCM(i1, i2);
		break;
	case 9:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		out = mathDotProd(_mathTest_Buffer + i1, _mathTest_Buffer + i2, i3);
		break;
	case 10:
		cnslRead("%d%d%d%d%d", &i1, &i2, &i3, &i4, &i5);
		ret = mathLimCorr(_mathTest_Buffer + i1, _mathTest_Buffer + i2, i3, _mathTest_Buffer + i4, i5);
	}
	_mathTest_Display(ret, out);
	return 0;
}

// Interactive maths test
void mathTest_Run()
{
	int		ret;

	cnslWrite("Welcome to mathsTest\n");
	cnslWrite("--------------------\n\n");
	cnslWrite("This is a testbench for MATHS module\n");
	system("PAUSE");
	cnslWrite("\n\n");
	do
	{
		ret = _mathTest_DoOp();
	}while(ret != -2);
}



#endif /* HK_TEST_MATH_H_ */
