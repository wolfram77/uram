/*
 * hkTest_Memory.h
 *
 *  Created on: 30-Mar-2013
 *   Completed: 02-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_MEMORY_H_
#define HK_TEST_MEMORY_H_

#include <hkTest.h>
#include <hkMemory.h>

// global variables
#define	MEMTEST_DISPLAY_BYTES		80
char	_memTest_Buffer[1024];
int		_memTest_funcCount = 4;
char*	_memTest_funcName[] = {"write", "copy", "copysafe", "copycircular"};
char*	_memTest_funcDesc[] = {
	"Pseudo String write function(string, src_address)",
	"memCopy(dest, src, size)",
	"memCopySafe(void* dest, void* src, int size)",
	"memCopyCirc(void* dest, int dest_sz, int dest_off, void* src, int src_sz, int src_off, int size)"
};

// Display the status of memory buffers
int _memTest_Display(int ret_val)
{
	cnslWrite("Memory status: %d ------\n", ret_val);
	tstDisplayChar("Buffer:  ", _memTest_Buffer, MEMTEST_DISPLAY_BYTES, " ");
	cnslWrite("\n");
	return 0;
}

// Accept one command and display status (-2 = exit requested)
int _memTest_DoOp()
{
	int		ret, func;
	char	func_name[256];
	int		i1, i2, i3, i4, i5, i6, i7;

	ret = 0;
	cnslWrite("(Enter command) mem: ");
	cnslRead("%s", func_name);
	if(_strcmpi(func_name, "exit") == 0) return -2;
	func = tstFuncNum(_memTest_funcName, _memTest_funcCount, func_name);
	if(func < 0)
	{
		cnslWrite("No such function.\n\n");
		return -1;
	}
	cnslWrite("%s\n", _memTest_funcDesc[func]);
	cnslWrite("Parameters: ");
	switch(func)
	{
	case 0:
		cnslRead("%s%d", func_name, &i1);
		memCopy(_memTest_Buffer + i1, func_name, strlen(func_name));
		break;
	case 1:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		memCopy(_memTest_Buffer + i1, _memTest_Buffer + i2, i3);
		break;
	case 2:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		ret = memCopySafe(_memTest_Buffer + i1, _memTest_Buffer + i2, i3);
		break;
	case 3:
		cnslRead("%d%d%d%d%d%d%d", &i1, &i2, &i3, &i4, &i5, &i6, &i7);
		ret = memCopyCirc(_memTest_Buffer + i1, i2, i3, _memTest_Buffer + i4, i5, i6, i7);
		break;
	}
	_memTest_Display(ret);
	return 0;
}

// Interactive memory test
void memTest_Run()
{
	int		ret;

	cnslWrite("Welcome to memoryTest\n");
	cnslWrite("--------------------\n\n");
	cnslWrite("This is a testbench for MEMORY module\n");
	system("PAUSE");
	cnslWrite("\n\n");
	do
	{
		ret = _memTest_DoOp();
	}while(ret != -2);
}



#endif /* HK_TEST_MEMORY_H_ */
