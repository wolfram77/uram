/*
 * hkTest_Semaphore.h
 *
 *  Created on: 30-Mar-2013
 *   Completed: 30-Mar-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_SEMAPHORE_H_
#define HK_TEST_SEMAPHORE_H_

#include <hkTest.h>
#include <hkSemaphore.h>

// global variables
int		_semTest_MutexCount = 8;
int		_semTest_Mutex[] =	{1, 2, 3, 4, 4, 3, 2, 1};
int		_semTest_funcCount =	4;
char*	_semTest_funcName[] = {"wait", "signal", "start", "stop"};
char*	_semTest_funcDesc[] = {
	"semWait(sem)",
	"semSignal(sem)",
	"semStart()",
	"semStop()",
};

// Display the status of semaphore
int _semTest_Display(int ret_val)
{
	cnslWrite("Semaphore status: %d ------\n", ret_val);
	cnslWrite("WaitCount: %4d\n", _semWaitCount);
	tstDisplayInt("Mutexes: ", _semTest_Mutex, _semTest_MutexCount, ", ");
	cnslWrite("\n");
	return 0;
}

// Accept one command and display status (-2 = exit requested)
int _semTest_DoOp()
{
	int		ret, func;
	char	func_name[256];
	int		i1;

	ret = 0;
	cnslWrite("(Enter command) sem: ");
	cnslRead("%s", func_name);
	if(_strcmpi(func_name, "exit") == 0) return -2;
	func = tstFuncNum(_semTest_funcName, _semTest_funcCount, func_name);
	if(func < 0)
	{
		cnslWrite("No such function.\n\n");
		return -1;
	}
	cnslWrite("%s\n", _semTest_funcDesc[func]);
	cnslWrite("Parameters: ");
	switch(func)
	{
	case 0:
		cnslRead("%d", &i1);
		if(i1 >= 0 && i1 < 8) semWait(_semTest_Mutex[i1])
		break;
	case 1:
		cnslRead("%d", &i1);
		if(i1 >= 0 && i1 < 8) semSignal(_semTest_Mutex[i1])
		break;
	case 2:
		ret = semStart();
		break;
	case 3:
		ret = semStop();
		break;
	}
	_semTest_Display(ret);
	return 0;
}

// Interactive semaphore test
void semTest_Run()
{
	int		ret;

	cnslWrite("Welcome to semaphoreTest\n");
	cnslWrite("--------------------\n\n");
	cnslWrite("This is a testbench for SEMAPHORE module\n");
	system("PAUSE");
	cnslWrite("\n\n");
	do
	{
		ret = _semTest_DoOp();
	}while(ret != -2);
}



#endif /* HK_TEST_SEMAPHORE_H_ */
