/*
 * hkTest_Ting.h
 *
 *  Created on: 04-Apr-2013
 *   Completed: 04-Apr-2013
 *      Author: Subhajit
 */

#pragma once

#ifndef HK_TEST_TING_H_
#define HK_TEST_TING_H_

#include <hkTest.h>
#include <hkTingModule.h>

// global constants
#define	TINGTEST_DISPLAY_COUNT		80
#define	TINGTEST_BUFFER_SIZE		256
#define	TINGTEST_QUEUE_SIZE			16
#define	TINGTEST_TX_FILE			"file_tx.txt"
#define	TINGTEST_RX_FILE			"file_rx.txt"

// global variables
char					_tingTest_Temp[1024];
char					_tingTest_Buffer[1024];
TING_HANDLE				_tingTest_hChannel[4];

int		_tingTest_funcCount = 16;
char*	_tingTest_funcName[] = {"wrt", "bin2code", "code2bin", "globalflush", "globalwrite", "globalqwrite", "globalread", "globalqread", "globalreadtemp", "globalqreadtemp", "open", "flush", "write", "read", "read_avail", "close"};
char*	_tingTest_funcDesc[] = {
	"Psuedo write(wrt) function(string, offset)",
	"_tingBin2Code(BYTE* src, int src_sz, BYTE* dest)",
	"_tingCode2Bin(BYTE* src, int src_sz, BYTE* dest)",
	"tingGlobalFlush()",
	"tingGlobalWrite(void* src, int src_sz)",
	"tingGlobalQwrite(FUNNEL_QUEUE* q_src, int block_sz)",
	"tingGlobalRead(void* dest, int dest_sz)",
	"tingGlobalQread(FUNNEL_QUEUE* q_dest, int block_sz)",
	"tingGlobalReadTemp(void* dest, int dest_sz)",
	"tingGlobalQreadTemp(FUNNEL_QUEUE* q_dest, int block_sz)",
	"tingOpen(short channel_type, int channel_sz)",
	"tingFlush(TING_CHANNEL_HANDLE channel)",
	"tingWrite(TING_CHANNEL_HANDLE channel, void* src, int write_sz)",
	"tingRead(TING_CHANNEL_HANDLE channel, void* dest, int read_sz)",
	"tingReadAvail(TING_CHANNEL_HANDLE channel, void* dest, int* dest_sz)",
	"tingClose(TING_CHANNEL_HANDLE channel)"
};

// Display the status of maths buffers
int _tingTest_Display(int ret_val)
{
	int		i;
	char	str[256];

	cnslWrite("\n");
	cnslWrite("Ting status: %d ------\n", ret_val);
	cnslWrite("Last Channel ID: %d\n", _tingLastChnlId);
	for(i=0; i<4, _tingTest_hChannel[i] != NULL; i++)
	{
		fqFront(&(_tingTest_hChannel[i]->Queue), _tingTest_hChannel[i]->Queue.Count, _tingTest_Temp);
		sprintf(str, "Channel %d: ", i);
		tstDisplayChar(str, _tingTest_Temp, _tingTest_hChannel[i]->Queue.Count, " ");
	}
	fqFront(&_tingTxQueue, _tingTxQueue.Count, _tingTest_Temp);
	tstDisplayChar("Ting Tx: ", _tingTest_Temp, _tingTxQueue.Count, " ");
	fqFront(&_tingRxQueue, _tingRxQueue.Count, _tingTest_Temp);
	tstDisplayChar("Ting Rx: ", _tingTest_Temp, _tingRxQueue.Count, " ");
	fqFront(&_tingChnlQueue, _tingChnlQueue.Count, _tingTest_Temp);
	tstDisplayHex("Ting Channel: ", _tingTest_Temp, _tingChnlQueue.Count, " ");
	tstDisplayHex("Ting Buffer: ", (char*)_tingBuffer.Data, TINGTEST_DISPLAY_COUNT, " ");
	tstDisplayHex("Ting ChBuffer: ", (char*)_tingChBuffer.Data, TINGTEST_DISPLAY_COUNT, " ");
	tstDisplayChar("Local Buffer:  ", _tingTest_Buffer, TINGTEST_DISPLAY_COUNT, " ");
	cnslWrite("\n");
	return 0;
}

// Accept one command and display status (-2 = exit requested)
int _tingTest_DoOp()
{
	int		ret, func;
	char	func_name[256];
	int		i1, i2, i3;

	ret = 0;
	cnslWrite("(Enter command) ting: ");
	cnslRead("%s", func_name);
	if(_strcmpi(func_name, "exit") == 0) return -2;
	func = tstFuncNum(_tingTest_funcName, _tingTest_funcCount, func_name);
	if(func < 0)
	{
		cnslWrite("No such function.\n\n");
		return -1;
	}
	cnslWrite("%s\n", _tingTest_funcDesc[func]);
	cnslWrite("Parameters: ");
	switch(func)
	{
	case 0:
		cnslRead("%s%d", func_name, &i1);
		strcpy(_tingTest_Buffer + i1, func_name);
		break;
	case 1:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		_tingBin2Code((BYTE*)_tingTest_Buffer + i1, i2, (BYTE*)_tingTest_Buffer + i3);
		break;
	case 2:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		_tingCode2Bin((BYTE*)_tingTest_Buffer + i1, i2, (BYTE*)_tingTest_Buffer + i3);
		break;
	case 3:
		cnslWrite("\n");
		ret = tingGlobalFlush();
		break;
	case 4:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalWrite(_tingTest_Buffer + i1, i2);
		break;
	case 5:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalQwrite(&(_tingTest_hChannel[i1]->Queue), i2);
		break;
	case 6:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalRead(_tingTest_Buffer + i1, i2);
		break;
	case 7:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalQread(&(_tingTest_hChannel[i1]->Queue), i2);
		break;
	case 8:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalReadTemp(_tingTest_Buffer + i1, i2);
		break;
	case 9:
		cnslRead("%d%d", &i1, &i2);
		ret = tingGlobalQreadTemp(&(_tingTest_hChannel[i1]->Queue), i2);
		break;
	case 10:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		_tingTest_hChannel[i1] = tingOpen((short)i2, i3);
		break;
	case 11:
		cnslRead("%d", &i1);
		ret = tingFlush(_tingTest_hChannel[i1]);
		break;
	case 12:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		ret = tingWrite(_tingTest_hChannel[i1], _tingTest_Buffer + i2, i3);
		break;
	case 13:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		ret = tingRead(_tingTest_hChannel[i1], _tingTest_Buffer + i2, i3);
		break;
	case 14:
		cnslRead("%d%d%d", &i1, &i2, &i3);
		ret = tingReadAvail(_tingTest_hChannel[i1], _tingTest_Buffer + i2, &i3);
		break;
	case 15:
		cnslRead("%d", &i1);
		ret = tingClose(_tingTest_hChannel[i1]);
	}
	_tingTest_Display(ret);
	return 0;
}

// Interactive TING test
int tingTest_Run()
{
	int		ret;

	cnslWrite("Welcome to tingTest\n");
	cnslWrite("--------------------\n\n");
	cnslWrite("This is a testbench for TING module\n");
	system("PAUSE");
	cnslWrite("\n\n");
	tingStart(TINGTEST_BUFFER_SIZE, TINGTEST_QUEUE_SIZE, TINGTEST_TX_FILE, TINGTEST_RX_FILE);
	do
	{
		ret = _tingTest_DoOp();
	}while(ret != -2);
	ret = tingStop();
	return ret;
}



#endif /* HK_TEST_TING_H_ */
