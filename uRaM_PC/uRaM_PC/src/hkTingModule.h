/*
 * hkTingModule.h
 *
 *  Created on: 06-Mar-2013
 *   Completed: 07-Apr-2013
 *      Author: Subhajit
 */

// #pragma once

#ifndef HK_TING_H_
#define HK_TING_H_

#include <hkConsoleIO.h>
#include <hkDataType.h>
#include <hkFunnelQueue.h>
#include <hkMath.h>
#ifndef	EXTERNAL_PROCESSOR
#include <Windows.h>
#endif

// TING Module
// Transmission of information
#define		TING_CHANNEL_TX				1
#define		TING_CHANNEL_RX				2
#define		TING_MAX_CHANNELS			16
#define		TING_QUEUE_MIN_SIZE			256
#define		TING_BUFFER_MIN_SIZE		1024
#define		TING_MAX_BYTES_READ			65536
#define		TING_MIN_CHANNEL_SIZE		64
#define		TING_PACKET_HEADER_SIZE		4

typedef struct _TING_CHANNEL
{
	short	Id;
	short	Type;
	FUNNEL_QUEUE	Queue;
}TING_CHANNEL;
typedef int		TING_HANDLE;

// global variables for ting
FUNNEL_QUEUE		_tingTxQueue;
FUNNEL_QUEUE		_tingRxQueue;
FUNNEL_QUEUE		_tingChnlQueue;
FUNNEL_QUEUE		_tingBuffer;
FUNNEL_QUEUE		_tingChBuffer;
#ifndef	EXTERNAL_PROCESSOR
HANDLE		_tingTxFile;
HANDLE		_tingRxFile;
#endif

// global macros
#define		_tingGetChannel(ch, hCh)		fqGetElem(&_tingChnlQueue, sizeof(TING_CHANNEL), hCh, &ch)

// convert a binary stream to a coded stream (multiple of 2 bytes)
void _tingBin2Code(BYTE* src, int src_sz, BYTE* dest)
{
	BYTE* src_end;
	
	src_end = src + src_sz;
	for(; src<src_end; src++, dest+=2)
	{
		*dest = (*src & 0xF) + 'A';
		dest[1] = (*src >> 4) + 'A';
	}
}

// convert a coded stream (multiple of 2 bytes) to a binary stream
void _tingCode2Bin(BYTE* src, int src_sz, BYTE* dest)
{
	BYTE* src_end;
	
	src_end = src + src_sz;
	for(; src<src_end; src+=2, dest++)
	{
		*dest = (*src - 'A') | ((src[1] - 'A') << 4);
	}
}

// start TING module
#ifdef	EXTERNAL_PROCESSOR
int tingStart(int buffer_sz, int fqsz)
#else
int tingStart(int buffer_sz, int fqsz, char* tx_file, char* rx_file)
#endif
{
	// prepare Buffers, Channel, Tx, Rx queues
	fqInit(&_tingBuffer, TING_BUFFER_MIN_SIZE, TING_BUFFER_MIN_SIZE, buffer_sz);
	fqInit(&_tingChBuffer, TING_BUFFER_MIN_SIZE, TING_BUFFER_MIN_SIZE, buffer_sz);
	fqInit(&_tingChnlQueue, sizeof(TING_CHANNEL), sizeof(TING_CHANNEL), TING_MAX_CHANNELS);
	fqInit(&_tingTxQueue, TING_QUEUE_MIN_SIZE, TING_QUEUE_MIN_SIZE, fqsz);
	fqInit(&_tingRxQueue, TING_QUEUE_MIN_SIZE, TING_QUEUE_MIN_SIZE, fqsz);
	_tingRxQueue.Static = 0;
#ifndef	EXTERNAL_PROCESSOR
	// open Tx, Rx files in shared mode
	_tingTxFile = CreateFile(tx_file, GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
									OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_TEMPORARY |
									FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH, NULL);
	if(_tingTxFile == INVALID_HANDLE_VALUE) _tingTxFile = NULL;
	_tingRxFile = CreateFile(rx_file, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
									OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_TEMPORARY |
									FILE_FLAG_SEQUENTIAL_SCAN | FILE_FLAG_WRITE_THROUGH, NULL);
	if(_tingRxFile == INVALID_HANDLE_VALUE) _tingRxFile = NULL;
#endif
	return 0;
}

// stop TING module
int tingStop()
{
	// free Buffers ,Channel, Tx, Rx queues
	fqClose(&_tingBuffer);
	fqClose(&_tingChBuffer);
	fqClose(&_tingChnlQueue);
	fqClose(&_tingTxQueue);
	fqClose(&_tingRxQueue);
#ifndef	EXTERNAL_PROCESSOR
	// Close Tx, Rx files
	if(_tingTxFile != NULL) CloseHandle(_tingTxFile);
	if(_tingRxFile != NULL) CloseHandle(_tingRxFile);
#endif
	return 0;
}

// flush TING Tx, Rx queues
int tingGlobalFlush()
{
	int		nBytes;
	short	pckPtr[2];
	TING_CHANNEL*	chnlPtr;

	// flush Tx data
	fqAccessBegin(&_tingTxQueue);
	nBytes = (_tingTxQueue.Count / _tingTxQueue.MinSize) * _tingTxQueue.MinSize;
	if(nBytes)
	{
		fqAccessBegin(&_tingBuffer);
		fqPop(&_tingTxQueue, nBytes, _tingBuffer.Data);
		_tingBin2Code(_tingBuffer.Data, nBytes, _tingBuffer.Data + nBytes);
#ifdef	EXTERNAL_PROCESSOR
		_tingBuffer.Data[nBytes + (nBytes << 1)] = '\0';
		cnslWriteStr(_tingBuffer.Data + nBytes);
#else
		WriteFile(_tingTxFile, _tingBuffer.Data + nBytes, nBytes << 1, (DWORD*)&nBytes, NULL);
#endif
		fqAccessEnd(&_tingBuffer);
	}
	fqAccessEnd(&_tingTxQueue);

	// flush Rx data
	fqAccessBegin(&_tingBuffer);
#ifdef	EXTERNAL_PROCESSOR
	char* status = cnslReadStr(_tingBuffer.Data);
	if(status == NULL) nBytes = 0;
	else nBytes = strlen(_tingBuffer.Data);
#else
	BOOL fstatus = ReadFile(_tingRxFile, _tingBuffer.Data, TING_MAX_BYTES_READ, (DWORD*)&nBytes, NULL);
	if(!fstatus) nBytes = 0;
#endif
	if(nBytes)
	{
		_tingCode2Bin(_tingBuffer.Data, nBytes, _tingBuffer.Data + nBytes);
		fqAccessBegin(&_tingRxQueue);
		fqPush(&_tingRxQueue, nBytes >> 1, _tingBuffer.Data + nBytes);
		while(1)
		{
			if(fqFront(&_tingRxQueue, TING_PACKET_HEADER_SIZE, pckPtr) < 0) break;
			if(pckPtr[1] > _tingRxQueue.Count) break;
			if(fqGetElem(&_tingChnlQueue, sizeof(TING_CHANNEL), pckPtr[0], chnlPtr) < 0) break;
			if(chnlPtr->Id != 0)
			{
				if(fqPop(&_tingRxQueue, TING_PACKET_HEADER_SIZE, NULL) < 0) break;
				if(fqQmove(&(chnlPtr->Queue), &_tingRxQueue, pckPtr[1] - TING_PACKET_HEADER_SIZE) < 0) break;
			}
			else fqPop(&_tingRxQueue, pckPtr[1], NULL);
		}
		fqAccessEnd(&_tingRxQueue);
	}
	fqAccessEnd(&_tingBuffer);
	return 0;
}

// write data to TING Tx queue
inline int tingGlobalWrite(void* src, int src_sz)
{
	if(fqPush(&_tingTxQueue, src_sz, src) == 0) return 0;
	tingGlobalFlush();
	return fqPush(&_tingTxQueue, src_sz, src);
}

// write data to TING Tx queue (from a queue)
inline int tingGlobalQwrite(FUNNEL_QUEUE* q_src, int block_sz)
{
	if(fqQmove(&_tingTxQueue, q_src, block_sz) == 0) return 0;
	tingGlobalFlush();
	return fqQmove(&_tingTxQueue, q_src, block_sz);
}

// read data from TING Rx queue
inline int tingGlobalRead(void* dest, int dest_sz)
{
	if(fqPop(&_tingRxQueue, dest_sz, dest) == 0) return 0;
	tingGlobalFlush();
	return fqPop(&_tingRxQueue, dest_sz, dest);
}

// read data from TING Rx queue (to a queue)
inline int tingGlobalQread(FUNNEL_QUEUE* q_dest, int block_sz)
{
	if(fqQmove(q_dest, &_tingRxQueue, block_sz) == 0) return 0;
	tingGlobalFlush();
	return fqQmove(q_dest, &_tingRxQueue, block_sz);
}

// read data from TING Rx queue without popping
inline int tingGlobalReadTemp(void* dest, int dest_sz)
{
	if(fqFront(&_tingRxQueue, dest_sz, dest) == 0) return 0;
	tingGlobalFlush();
	return fqFront(&_tingRxQueue, dest_sz, dest);
}

// read data from TING Rx queue without popping (to a queue)
inline int tingGlobalQreadTemp(FUNNEL_QUEUE* q_dest, int block_sz)
{
	if(fqQcopy(q_dest, &_tingRxQueue, block_sz) == 0) return 0;
	tingGlobalFlush();
	return fqQcopy(q_dest, &_tingRxQueue, block_sz);
}

// opens a new TING channel
TING_HANDLE tingOpen(short channel_id, short channel_type, int channel_sz)
{
	TING_CHANNEL	channel;
	TING_HANDLE		hChannel;

	channel.Id = channel_id;
	channel.Type = channel_type;
	fqInit(&(channel.Queue), TING_MIN_CHANNEL_SIZE, TING_MIN_CHANNEL_SIZE, channel_sz + TING_PACKET_HEADER_SIZE);
	if(channel.Type == TING_CHANNEL_RX) channel.Queue.Static = 0;
	fqAccessBegin(&_tingChnlQueue);
	fqPush(&_tingChnlQueue, sizeof(TING_CHANNEL), &channel);
	hChannel = _tingChnlQueue.Count - 1;
	fqAccessEnd(&_tingChnlQueue);
	return hChannel;
}

// flush a Tx type TING channel
int tingFlush(TING_HANDLE hChannel)
{
	int				nBytes;
	short			pckPtr[2];
	TING_CHANNEL*	channel;

	_tingGetChannel(channel, hChannel);
	// for Tx type channel
	if(channel->Type == TING_CHANNEL_TX)
	{
		if(channel->Queue.Count == 0) return 0;
		nBytes = channel->Queue.Count;
		pckPtr[0] = channel->Id;
		pckPtr[1] = (short)(nBytes + TING_PACKET_HEADER_SIZE);
		tingGlobalWrite(pckPtr, TING_PACKET_HEADER_SIZE);
		tingGlobalQwrite(&(channel->Queue), nBytes + TING_PACKET_HEADER_SIZE);
		return 0;
	}
	return -1;
}

// write data to a TING channel
inline int tingWrite(TING_HANDLE hChannel, void* src, int write_sz)
{
	TING_CHANNEL*	channel;

	_tingGetChannel(channel, hChannel);
	if(fqPush(&(channel->Queue), write_sz, src) == 0) return 0;
	tingFlush(hChannel);
	return fqPush(&(channel->Queue), write_sz, src);
}

// read data from a TING channel
int tingRead(TING_HANDLE hChannel, void* dest, int read_sz)
{
	TING_CHANNEL*	channel;

	_tingGetChannel(channel, hChannel);
	return fqPop(&(channel->Queue), read_sz, dest);
}

// read all available data from a TING channel (returns bytes read in dest_sz)
int tingReadAvail(TING_HANDLE hChannel, void* dest, int* dest_sz)
{
	int		ret;
	TING_CHANNEL*	channel;
	
	_tingGetChannel(channel, hChannel);
	if(channel->Queue.Count > 0)
	{
		*dest_sz = mathMax(*dest_sz, channel->Queue.Count);
		ret = fqPop(&(channel->Queue), *dest_sz, dest);
		if(ret == 0) return 0;
	}
	*dest_sz = 0;
	return -1;
}

// close a TING channel
int tingClose(TING_HANDLE hChannel)
{
	TING_CHANNEL*	channel;

	_tingGetChannel(channel, hChannel);
	tingFlush(hChannel);
	channel->Id = 0;
	fqClose(&(channel->Queue));
	return 0;
}



#endif /* HK_TING_H_ */
