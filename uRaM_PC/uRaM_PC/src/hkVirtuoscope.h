/*
 * virtuoscope.h
 *
 *  Created on: 16-Mar-2013
 *   Completed: 17-Mar_2013
 *      Author: Subhajit
 */

#pragma once

#ifndef VIRTUOSCOPE_H_
#define VIRTUOSCOPE_H_

#include <Windows.h>
#include <hkMemory.h>
#include <hkFunnelQueue.h>

typedef struct _VIRTUOSCOPE
{
	char*			FileName;
	HANDLE			File;
	char			Mode;
	int				Tframe;
	int				FrameSz;
	FUNNEL_QUEUE	Data;
	void*			Buffer;
	int				Wait;
}VIRTUOSCOPE;

int vsoInit(VIRTUOSCOPE* scope, char* vso_file, char mode, int frame_sz)
{
	if(mode == 'r') scope->File = CreateFile(vso_file, GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	else scope->File = CreateFile(vso_file, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	if(scope->File == INVALID_HANDLE_VALUE) return -1;
	scope->FileName = vso_file;
	scope->Mode = mode;
	scope->FrameSz = frame_sz;
	scope->Tframe = 0;
	scope->Wait = 0;
	if(mode == 'r')	fqInit(&(scope->Data), frame_sz, sizeof(float), frame_sz);
	else fqInit(&(scope->Data), sizeof(float), frame_sz, frame_sz);
	scope->Data.Static = 0;
	scope->Buffer = memAlloc(frame_sz, char);
	return 0;
}

int vsoClose(VIRTUOSCOPE* scope)
{
	if(scope->File != INVALID_HANDLE_VALUE) CloseHandle(scope->File);
	fqClose(&(scope->Data));
	memFree(scope->Buffer);
	return 0;
}

int vsoReadData(VIRTUOSCOPE* scope)
{
	int		f_tframe;
	DWORD	num_bytes_read;

	if(scope->Mode != 'r') return -1;
	SetFilePointer(scope->File, 0, NULL, FILE_BEGIN);
	ReadFile(scope->File, scope->Buffer, 8, &num_bytes_read, NULL);
	if(num_bytes_read < 8) return -1;
	f_tframe = *((int*)scope->Buffer);
	if(f_tframe == scope->Tframe) return -1;
	scope->Tframe = f_tframe;
	scope->FrameSz = ((int*)scope->Buffer)[1];
	ReadFile(scope->File, scope->Buffer, scope->FrameSz, &num_bytes_read, NULL);
	fqPush(&(scope->Data), num_bytes_read, scope->Buffer);
	return 0;
}

inline int vsoRead(VIRTUOSCOPE* scope, float* val)
{
	int		ret;

	ret = fqPop(&(scope->Data), sizeof(float), val);
	if(ret < 0 || scope->Wait >= (scope->FrameSz<<2))
	{
		vsoReadData(scope);
		scope->Wait = 0;
		ret = fqPop(&(scope->Data), sizeof(float), val);
	}
	scope->Wait++;
	return ret;
}

int vsoWriteData(VIRTUOSCOPE* scope)
{
	if(scope->Mode != 'w') return -1;
	if(scope->Data.Count >= scope->FrameSz)
	{
		fqPop(&(scope->Data), scope->FrameSz, scope->Buffer);
		SetFilePointer(scope->File, 0, NULL, FILE_BEGIN);
		WriteFile(scope->File, scope->Buffer, scope->FrameSz, NULL, NULL);
	}
	return 0;
}

inline int vsoWrite(VIRTUOSCOPE* scope, float value)
{
	return fqPush(&(scope->Data), sizeof(float), &value);
}


#endif /* VIRTUOSCOPE_H_ */
