/*  
 *  Copyright 2003 by Texas Instruments Incorporated.  
 *  All rights reserved. Property of Texas Instruments Incorporated.  
 *  Restricted rights to use, duplicate or disclose this code are  
 *  granted through contract.  
 *    
 */   
/* "@(#) DDK 1.00.00.02 01-20-03 (ddk-a14)" */   
/*   
 *  ======== aic23.c ========   
 *  
 *  AIC23 codec driver implementation specific to the   
 *  Spectrum Digital EVM5509 board.  
 */   
   
#include <std.h>   
   
#include <csl.h>   
#include <csl_i2c.h>   
   
#include <aic23.h>         
   
/* The I2C slave address of the AIC23 */   
#define AIC23_I2CADDR        0x1A   
   
/* a flag indicating whether the codec has been initialized */   
static Bool codecInitialized = FALSE;   
   
/* function for writing a control word to the AIC23 thru I2C */   
static Void i2cWriteCtrl( Uns regaddr, Uns data );   
   
   
/*  
 *  ======== AIC23_init ========  
 *  
 *  Initializes codec module variables, if any. (There are none.)  
 */   
#pragma CODE_SECTION(AIC23_init, ".text:init")   
Void AIC23_init()   
{   
}       
   
/*  
 *  ======== AIC23_setParams ========  
 *  
 *  This function takes a pointer to the object of type AIC23_Params,  
 *  and writes all 11 control words found in it to the codec. Prior  
 *  to that it initializes the codec if this is the first time the  
 *  function is ever called.  
 *  The 16-bit word is composed of register address in the upper 7 bits  
 *  and the 9-bit register value stored in the parameters structure.  
 */   
Int AIC23_setParams( AIC23_Params *params )   
{   
    Uns i;   
       
    /*   
     * I2C structure which will be used to send the data */   
    I2C_Setup i2cSetup = {   
        0,              /* 7 bit address mode */   
        0x007F,         /* own address - dont care if master */   
        75,             /* clkout value (Mhz) */   
        400,            /* a number between 10 and 400 */   
        0,              /* num of bits/bytes to be received/transmitted (8) */   
        0,              /* DLB mode off */   
        0               /* FREE mode of operation off */   
    };   
   
    if (codecInitialized == FALSE) {   
        codecInitialized = TRUE;   
           
        /* setup config for I2C */   
        I2C_setup(&i2cSetup);   
   
        i2cWriteCtrl(15, 0);   
    }   
       
    for (i = 0; i < AIC23_NUMREGS; i++) {           
        i2cWriteCtrl(i, (params->regs[i] & 0x1ff));   
    }   
       
    return 0;   
}   
   
/*  
 *  ======== i2cWriteCtrl ========  
 *  
 *  This function writes a control word to AIC23 reg thru I2C port   
 */   
static Void i2cWriteCtrl(Uns regaddr, Uns data)   
{   
    Uint16 buf[2], i;   
       
    buf[0] = (Uint16)((regaddr<<1)+(data>>8));   
    buf[1] = (Uint16)(data & 0x00FF);   
    I2C_write(buf, 2, 1, AIC23_I2CADDR, 1, 100);   
       
    for (i = 0; i < 1000; i++);   
}
