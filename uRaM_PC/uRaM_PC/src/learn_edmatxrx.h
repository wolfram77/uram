/*  
 *  Copyright 2002 by Spectrum Digital Incorporated.  
 *  All rights reserved.  Property of Spectrum Digital Incorporated.  
 */   
   
/*  
 *  ======== evm_app.c ========  
 *  
 *  Version 0.80 (beta)  
 *  
 *  This example digitally processes audio data from the line input on the  
 *  AIC23 codec and plays the result on the line output and headphone output.  
 *  It uses the McBSP and DMA to efficiently handle the data transfer without  
 *  significant intervention from the DSP.  
 *  
 *  
 *  Data transfer  
 *  
 *  Audio data is transferred back and forth from the codec through McBSP1,  
 *  a bidirectional serial port.  The DMA is configured to take every 16-bit  
 *  signed audio sample arriving on McBSP1 and store it in a buffer in memory  
 *  until it can be processed.  Once it has been processed, it is sent back out  
 *  through McBSP1 to the codec for output.  DMA channel 4 is used for  
 *  transmitting data to the codec and channel 5 is used to receive data from  
 *  the codec.  
 *  
 *  The AIC23 codec is configured through the I2C interface.  
 *  
 *  
 *  Program flow  
 *  
 *  When the program is run, the individual DSP/BIOS modules are initialized  
 *  as configured in evm_app.cdb with the DSP/BIOS configuration tool.  The  
 *  main() function is then called as the main user thread.  In this example  
 *  main() performs application initialization and starts the DMA data  
 *  transfers.  When main exits, control passes back entirely to DSP/BIOS  
 *  which services any interrupts or threads on an as-needed basis.  When  
 *  there is no work to be done, the idle thread is run.  
 *  
 *  The dmaHwi() interrupt service routine is called when a buffer has been  
 *  filled.  It contains a state variable named pingOrPong that indicates  
 *  whether the buffer is a PING or PONG buffer.  dmaHwi switches the buffer  
 *  state to the opposite buffer and calls the SWI thread processBuffer to  
 *  process the audio data.  
 *  
 *  Please see the 5502 EVM help file under Software/Examples for more  
 *  detailed information on this example.  
 */   
   
/*  
 *  DSP/BIOS is configured using the DSP/BIOS configuration tool.  Settings  
 *  for this example are stored in a configuration file called evm_app.cdb.  
 *  At compile time, Code Composer will auto-generate DSP/BIOS related files  
 *  based on these settings.  A header file called evm_appcfg.h contains the  
 *  results of the autogeneration and must be included for proper operation.  
 *  The name of the file is taken from evm_app.cdb and adding cfg.h.  
 */   
#include "evm_appcfg.h"   
   
#include "aic23.h"   
   
#include "evm5502.h"   
#include "evm5502_led.h"   
#include "evm5502_dip.h"   
   
/*  
 *  This program uses Code Composer's Chip Support Library to access   
 *  C55x peripheral registers and interrupt setup.  The following   
 *  include files are required for the CSL modules.  
 */   
#include <csl.h>   
#include <csl_irq.h>   
#include <csl_dma.h>   
#include <csl_mcbsp.h>   
   
/* Constants for the buffered ping-pong transfer */   
#define BUFFSIZE          96   
#define PING              0   
#define PONG              1   
   
/* Function prototypes */   
void initIrq(void);   
void initDma(void);   
void copyData(Int16 *inbuf, Int16 *outbuf, Int16 length);   
void processBuffer(void);   
void blinkLED(void);   
void blinkLED0(void);   
void load(void);   
void powerDown(void);   
void dmaHwi(void);   
   
/*  Handles  */   
DMA_Handle hDmaXmt;   
DMA_Handle hDmaRcv;   
MCBSP_Handle hMcbsp1;   
   
/*  Config Structures */   
/* CSL config structure for DMAs */   
static DMA_Config dmaCfgReceive = {   
    DMA_DMACSDP_RMK(   
        DMA_DMACSDP_DSTBEN_NOBURST,   
        DMA_DMACSDP_DSTPACK_OFF,   
        DMA_DMACSDP_DST_DARAM,   
        DMA_DMACSDP_SRCBEN_NOBURST,   
        DMA_DMACSDP_SRCPACK_OFF,   
        DMA_DMACSDP_SRC_PERIPH,   
        DMA_DMACSDP_DATATYPE_16BIT   
    ),                    /* DMACDSP */   
    DMA_DMACCR_RMK(   
        DMA_DMACCR_DSTAMODE_POSTINC,   
        DMA_DMACCR_SRCAMODE_CONST,   
        DMA_DMACCR_ENDPROG_OFF,   
        DMA_DMACCR_REPEAT_OFF,   
        DMA_DMACCR_AUTOINIT_OFF,   
        DMA_DMACCR_EN_STOP,   
        DMA_DMACCR_PRIO_HI,   
        DMA_DMACCR_FS_DISABLE,   
        DMA_DMACCR_SYNC_REVT1   
    ),                    /* DMACCR */            
    DMA_DMACICR_RMK(   
        DMA_DMACICR_BLOCKIE_OFF,   
        DMA_DMACICR_LASTIE_OFF,   
        DMA_DMACICR_FRAMEIE_ON,   
        DMA_DMACICR_FIRSTHALFIE_OFF,   
        DMA_DMACICR_DROPIE_OFF,   
        DMA_DMACICR_TIMEOUTIE_OFF   
    ),                   /* DMACICR */        
    (DMA_AdrPtr)((Uint32)(_MCBSP_DRR11_ADDR<<1)), /* DMACSSAL */   
    0,                   /* DMACSSAU                      */   
    NULL,                /* DMACDSAL, to be loaded by submit  */   
    0,                   /* DMACDSAU                      */   
    BUFFSIZE,            /* DMACEN                        */   
    1,                   /* DMACFN                        */   
    0,                   /* DMACFI                        */   
    0                    /* DMACEI                        */   
};   
   
static DMA_Config dmaCfgTransmit = {   
    DMA_DMACSDP_RMK(   
        DMA_DMACSDP_DSTBEN_NOBURST,   
        DMA_DMACSDP_DSTPACK_OFF,   
        DMA_DMACSDP_DST_PERIPH,   
        DMA_DMACSDP_SRCBEN_NOBURST,   
        DMA_DMACSDP_SRCPACK_OFF,   
        DMA_DMACSDP_SRC_DARAM,   
        DMA_DMACSDP_DATATYPE_16BIT   
    ),                   /* DMACDSP */   
    DMA_DMACCR_RMK(   
        DMA_DMACCR_DSTAMODE_CONST,   
        DMA_DMACCR_SRCAMODE_POSTINC,   
        DMA_DMACCR_ENDPROG_OFF,   
        DMA_DMACCR_REPEAT_OFF,   
        DMA_DMACCR_AUTOINIT_OFF,   
        DMA_DMACCR_EN_STOP,   
        DMA_DMACCR_PRIO_HI,   
        DMA_DMACCR_FS_DISABLE,   
        DMA_DMACCR_SYNC_XEVT1   
    ),                   /* DMACCR */   
    DMA_DMACICR_RMK(   
        DMA_DMACICR_BLOCKIE_OFF,   
        DMA_DMACICR_LASTIE_OFF,   
        DMA_DMACICR_FRAMEIE_ON,   
        DMA_DMACICR_FIRSTHALFIE_OFF,   
        DMA_DMACICR_DROPIE_OFF,   
        DMA_DMACICR_TIMEOUTIE_OFF   
    ),                   /* DMACICR */   
    NULL,                /* DMACSSAL */   
    0,                   /* DMACSSAU                     */   
    (DMA_AdrPtr)((Uint32)(_MCBSP_DXR11_ADDR<<1)),/* DMACDSAL */   
    0,                   /* DMACDSAU                     */   
    BUFFSIZE,            /* DMACEN                       */   
    1,                   /* DMACFN                       */   
    0,                   /* DMACFI                       */   
    0                    /* DMACEI                       */   
};   
   
/* CSL config structure for MCBSP 1 */   
static MCBSP_Config mcbspCfg1 = {   
    MCBSP_SPCR1_RMK(   
        MCBSP_SPCR1_DLB_OFF,                   /* DLB      = 0 */   
        MCBSP_SPCR1_RJUST_RZF,                 /* RJUST    = 0 */   
        MCBSP_SPCR1_CLKSTP_DISABLE,            /* CLKSTP   = 0 */   
        MCBSP_SPCR1_DXENA_NA,                  /* DXENA    = 0 */   
        MCBSP_SPCR1_ABIS_DISABLE,              /* ABIS     = 0 */   
        MCBSP_SPCR1_RINTM_RRDY,                /* RINTM    = 0 */   
        0,                                     /* RSYNCER  = 0 */   
        0,                                     /* RFULL    = 0 */   
        0,                                     /* RRDY     = 0 */   
        MCBSP_SPCR1_RRST_DISABLE               /* RRST     = 0 */   
    ),   
    MCBSP_SPCR2_RMK(   
        MCBSP_SPCR2_FREE_YES,                  /* FREE     = 1 */   
        MCBSP_SPCR2_SOFT_YES,                  /* SOFT     = ` */   
        MCBSP_SPCR2_FRST_FSG,                  /* FRST     = 0 */   
        MCBSP_SPCR2_GRST_CLKG,                 /* GRST     = 0 */   
        MCBSP_SPCR2_XINTM_XRDY,                /* XINTM    = 0 */   
        0,                                     /* XSYNCER  = 0 */   
        0,                                     /* XEMPTY   = 0 */   
        0,                                     /* XRDY     = 0 */   
        MCBSP_SPCR2_XRST_DISABLE               /* XRST     = 0 */   
    ),   
    MCBSP_RCR1_RMK(    
        MCBSP_RCR1_RFRLEN1_OF(1),              /* RFRLEN1  = 1 */   
        MCBSP_RCR1_RWDLEN1_16BIT               /* RWDLEN1  = 2 */   
    ),   
    MCBSP_RCR2_RMK(       
        MCBSP_RCR2_RPHASE_SINGLE,              /* RPHASE   = 0 */   
        MCBSP_RCR2_RFRLEN2_OF(0),              /* RFRLEN2  = 0 */   
        MCBSP_RCR2_RWDLEN2_8BIT,               /* RWDLEN2  = 0 */   
        MCBSP_RCR2_RCOMPAND_MSB,               /* RCOMPAND = 0 */   
        MCBSP_RCR2_RFIG_YES,                   /* RFIG     = 0 */   
        MCBSP_RCR2_RDATDLY_0BIT                /* RDATDLY  = 0 */   
    ),     
    MCBSP_XCR1_RMK(       
        MCBSP_XCR1_XFRLEN1_OF(1),              /* XFRLEN1  = 1 */    
        MCBSP_XCR1_XWDLEN1_16BIT               /* XWDLEN1  = 2 */   
    ),      
    MCBSP_XCR2_RMK(      
        MCBSP_XCR2_XPHASE_SINGLE,              /* XPHASE   = 0 */   
        MCBSP_XCR2_XFRLEN2_OF(0),              /* XFRLEN2  = 0 */   
        MCBSP_XCR2_XWDLEN2_8BIT,               /* XWDLEN2  = 0 */   
        MCBSP_XCR2_XCOMPAND_MSB,               /* XCOMPAND = 0 */   
        MCBSP_XCR2_XFIG_YES,                   /* XFIG     = 0 */   
        MCBSP_XCR2_XDATDLY_0BIT                /* XDATDLY  = 0 */   
    ),               
    MCBSP_SRGR1_RMK(    
        MCBSP_SRGR1_FWID_OF(0),                /* FWID     = 0 */   
        MCBSP_SRGR1_CLKGDV_OF(0)               /* CLKGDV   = 0 */   
    ),      
    MCBSP_SRGR2_RMK(     
        MCBSP_SRGR2_GSYNC_FREE,                /* FREE     = 0 */   
        MCBSP_SRGR2_CLKSP_RISING,              /* CLKSP    = 0 */   
        MCBSP_SRGR2_CLKSM_CLKS,                /* CLKSM    = 0 */   
        MCBSP_SRGR2_FSGM_DXR2XSR,              /* FSGM     = 0 */   
        MCBSP_SRGR2_FPER_OF(0)                 /* FPER     = 0 */   
    ),     
    MCBSP_MCR1_DEFAULT,   
    MCBSP_MCR2_DEFAULT,    
    MCBSP_PCR_RMK(   
        MCBSP_PCR_IDLEEN_RESET,                /* IDLEEN   = 0 */   
        MCBSP_PCR_XIOEN_SP,                    /* XIOEN    = 0 */   
        MCBSP_PCR_RIOEN_SP,                    /* RIOEN    = 0 */   
        MCBSP_PCR_FSXM_EXTERNAL,               /* FSXM     = 0 */   
        MCBSP_PCR_FSRM_EXTERNAL,               /* FSRM     = 0 */   
        MCBSP_PCR_SCLKME_NO,                   /* SCLKME   = 0 */   
        0,                                     /* CLKSSTAT = 0 */   
        0,                                     /* DXSTAT   = 0 */   
        0,                                     /* DRSTAT   = 0 */   
        MCBSP_PCR_CLKXM_INPUT,                 /* CLKXM    = 0 */   
        MCBSP_PCR_CLKRM_INPUT,                 /* CLKRM    = 0 */   
        MCBSP_PCR_FSXP_ACTIVEHIGH,             /* FSXP     = 0 */   
        MCBSP_PCR_FSRP_ACTIVEHIGH,             /* FSRP     = 0 */   
        MCBSP_PCR_CLKXP_FALLING,               /* CLKXP    = 1 */   
        MCBSP_PCR_CLKRP_RISING                 /* CLKRP    = 1 */   
    ),   
    MCBSP_RCERA_DEFAULT,    
    MCBSP_RCERB_DEFAULT,    
    MCBSP_RCERC_DEFAULT,    
    MCBSP_RCERD_DEFAULT,    
    MCBSP_RCERE_DEFAULT,    
    MCBSP_RCERF_DEFAULT,    
    MCBSP_RCERG_DEFAULT,    
    MCBSP_RCERH_DEFAULT,    
    MCBSP_XCERA_DEFAULT,   
    MCBSP_XCERB_DEFAULT,   
    MCBSP_XCERC_DEFAULT,   
    MCBSP_XCERD_DEFAULT,     
    MCBSP_XCERE_DEFAULT,   
    MCBSP_XCERF_DEFAULT,     
    MCBSP_XCERG_DEFAULT,   
    MCBSP_XCERH_DEFAULT   
};   
   
/* Codec configuration settings */   
AIC23_Params config = { \   
    0x001c,  /* 0 EVM5502_AIC23_LEFTINVOL  Left line input channel volume */ \   
    0x001c,  /* 1 EVM5502_AIC23_RIGHTINVOL Right line input channel volume */\   
    0x01f9,  /* 2 EVM5502_AIC23_LEFTHPVOL  Left channel headphone volume */  \   
    0x01f9,  /* 3 EVM5502_AIC23_RIGHTHPVOL Right channel headphone volume */ \   
    0x0010,  /* 4 EVM5502_AIC23_ANAPATH    Analog audio path control */      \   
    0x0000,  /* 5 EVM5502_AIC23_DIGPATH    Digital audio path control */     \   
    0x0000,  /* 6 EVM5502_AIC23_POWERDOWN  Power down control */             \   
    0x0043,  /* 7 EVM5502_AIC23_DIGIF      Digital audio interface format */ \   
    0x0001,  /* 8 EVM5502_AIC23_SAMPLERATE Sample rate control */            \   
    0x0001   /* 9 EVM5502_AIC23_DIGACT     Digital interface activation */   \   
};   
   
   
/* The eighth register of CODEC determine the Codec sample rate  
 * Refrence the following table:  
 *       Sample Rate     REG VALUE            
 * ---------------------------------------------  
 *          96KHz       0X001D/0X009D  
 *          48KHz       0X0001/0X0081  
 *          44.1KHz     0X0023/0X00A3  
 *          32KHz       0X0019/0X0099  
 *          24KHz       0X0041/0X00C1  
 *          16KHz       0X0059/0X00D9  
 *          8KHz        0X000D/0X008D  
 * ----------------------------------------------  
 */   
   
   
   
/*  
 * Data buffer declarations - the program uses four logical buffers of size  
 * BUFFSIZE, one ping and one pong buffer on both receive and transmit sides.  
 */   
#pragma DATA_SECTION (gBufferXmtPing, "buffer_sect");   
Int16 gBufferXmtPing[BUFFSIZE];  // Transmit PING buffer   
#pragma DATA_SECTION (gBufferXmtPong, "buffer_sect");   
Int16 gBufferXmtPong[BUFFSIZE];  // Transmit PONG buffer   
#pragma DATA_SECTION (gBufferRcvPing, "buffer_sect");   
Int16 gBufferRcvPing[BUFFSIZE];  // Receive PING buffer   
#pragma DATA_SECTION (gBufferRcvPong, "buffer_sect");   
Int16 gBufferRcvPong[BUFFSIZE];  // Receive PONG buffer   
   
/* Event IDs, global so they can be set in initIrq() and used everywhere */   
Uint16 eventIdRcv;   
Uint16 eventIdXmt;   
   
Int32 count = 0;   
   
void blinkLED()   
{   
    /* Toggle LED #2 and #3 */   
    EVM5502_LED_toggle(2);   
    EVM5502_LED_toggle(3);   
}   
   
   
   
void blinkLED0()   
{   
    /* Check DIP switch #0 and light LED #0 accordingly, 0 = switch pressed */   
    if (EVM5502_DIP_get(0) == 0)   
        /* Switch pressed, togglw LED #0  */   
        EVM5502_LED_toggle(0);   
    else   
        /* Switch not pressed, turn LED #0 off */   
        EVM5502_LED_off(0);   
}   
   
   
/* ------------------------Helper Functions ----------------------------- */    
   
/*  
 *  initMcbsp() - Initialize the McBSP for codec data transfers using the  
 *                configuration define at the top of this file.  
 */   
void initMcbsp()   
{   
    /* Open the codec data McBSP */   
    hMcbsp1 = MCBSP_open(MCBSP_PORT1, MCBSP_OPEN_RESET);   
   
    /* Configure the codec to match the AIC23 data format */   
    MCBSP_config(hMcbsp1, &mcbspCfg1);   
   
    /* Clear any garbage from the codec data port */   
    if (MCBSP_rrdy(hMcbsp1))   
        MCBSP_read16(hMcbsp1);   
           
    /* Start the McBSP running */   
    MCBSP_start(hMcbsp1, MCBSP_XMIT_START | MCBSP_RCV_START |   
        MCBSP_SRGR_START | MCBSP_SRGR_FRAMESYNC, 220);   
}   
   
/*  
 *  initIrq() - Initialize and enable the DMA receive interrupt using the CSL.  
 *              The interrupt service routine for this interrupt is hwiDma.  
 */   
void initIrq(void)   
{   
    // Get Event ID associated with DMA channel interrupt.  Event IDs are a   
    // CSL abstraction that lets code describe a logical event that gets   
    // mapped to a real physical event at run time.  This helps to improve   
    // code portability.   
    eventIdRcv = DMA_getEventId(hDmaRcv);   
    eventIdXmt = DMA_getEventId(hDmaXmt);   
       
    // Clear any pending receive channel interrupts (IFR)   
    IRQ_clear(eventIdRcv);   
    IRQ_clear(eventIdXmt);   
    
    // Enable receive DMA interrupt (IMR)   
    IRQ_enable(eventIdRcv);    
       
    // Make sure global interrupts are enabled   
    IRQ_globalEnable();   
}   
   
   
/*  
 *  initDma() - Initialize the DMA controller.  
 */   
void initDma(void)   
{   
    volatile Int16 i;   
   
    /* Set transmit and receive buffer addresses in config structures */   
    dmaCfgReceive.dmacdsal = (DMA_AdrPtr)(((Uint32)(&gBufferRcvPing) << 1) & 0xFFFF);   
    dmaCfgReceive.dmacdsau = (Uint16)((Uint32)(&gBufferRcvPing) >> 15);   
    dmaCfgTransmit.dmacssal = (DMA_AdrPtr)(((Uint32)(&gBufferXmtPing) << 1) & 0xFFFF);   
    dmaCfgTransmit.dmacssau = (Uint16)((Uint32)(&gBufferXmtPing) >> 15);   
   
    /* Open DMA channels */   
    hDmaXmt = DMA_open(DMA_CHA5, DMA_OPEN_RESET);   
    hDmaRcv = DMA_open(DMA_CHA4, DMA_OPEN_RESET);   
          
    /* Configure DMA channels */   
    DMA_config(hDmaXmt, &dmaCfgTransmit);   
    DMA_config(hDmaRcv, &dmaCfgReceive);   
           
    /* Clear the DMA status registers to receive new interrupts */   
    i = DMA_RGETH(hDmaRcv, DMACSR);   
    i = DMA_RGETH(hDmaXmt, DMACSR);   
   
    DMA_RSET(DMAGCR, 0x4);   
       
    /* Clear any garbage from the codec data port */   
    if (MCBSP_rrdy(hMcbsp1))   
        MCBSP_read16(hMcbsp1);   
           
    // Start the DMA   
    DMA_start(hDmaRcv);   
    DMA_start(hDmaXmt);   
}   
   
   
/*  
 *  copyData() - Copy one buffer with length elements to another.  
 */   
void copyData(Int16 *inbuf, Int16 *outbuf, Int16 length)   
{   
    Int16 i = 0;   
       
    for (i = 0; i < length; i++) {                         
        outbuf[i]  = inbuf[i];   
    }   
}   
   
   
/* ------------------------------- Threads ------------------------------ */   
   
/*  
 *  processBuffer() - Process audio data once it has been received.              
 */   
void processBuffer(void)   
{   
    Uint32 addr;   
    Int16 pingPong;   
   
    // Wait until transmit DMA is finished too   
//    while(!IRQ_test(eventIdXmt));   
       
    // Pong-pong state passed through mailbox from dmaHwi()   
    pingPong = SWI_getmbox();   
   
    // Determine which ping-pong state we're in   
    if (pingPong == PING)   
    {   
        copyData(gBufferRcvPing, gBufferXmtPing, BUFFSIZE);   
   
        // Configure the receive channel for ping input data   
        addr = ((Uint32)gBufferRcvPing) << 1;   
        DMA_RSETH(hDmaRcv, DMACDSAL, addr & 0xffff);   
        DMA_RSETH(hDmaRcv, DMACDSAU, (addr >> 16) & 0xffff);   
   
        // Configure the transmit channel for ping output data   
        addr = ((Uint32)gBufferXmtPing) << 1;       
        DMA_RSETH(hDmaXmt, DMACSSAL, addr & 0xffff);   
        DMA_RSETH(hDmaXmt, DMACSSAU, (addr >> 16) & 0xffff);       
    }   
    else {   
        copyData(gBufferRcvPong, gBufferXmtPong, BUFFSIZE);   
   
        // Configure the receive channel for pong input data   
        addr = ((Uint32)gBufferRcvPong) << 1;   
        DMA_RSETH(hDmaRcv, DMACDSAL, addr & 0xffff);   
        DMA_RSETH(hDmaRcv, DMACDSAU, (addr >> 16) & 0xffff);   
   
        // Configure the transmit channel for pong output data   
        addr = ((Uint32)gBufferXmtPong) << 1;       
        DMA_RSETH(hDmaXmt, DMACSSAL, addr & 0xffff);   
        DMA_RSETH(hDmaXmt, DMACSSAU, (addr >> 16) & 0xffff);   
    }   
       
    // Start the DMA   
    DMA_start(hDmaRcv);   
    DMA_start(hDmaXmt);   
    if ( MCBSP_xrdy(hMcbsp1) )   
        MCBSP_write16(hMcbsp1, 0);   
//    MCBSP_read16(hMcbsp1);   
}   
   
   
/* ---------------------- Interrupt Service Routines -------------------- */   
/*  
 *  dmaHwi() - Interrupt service routine for the DMA transfer.  It is triggered  
 *             when a DMA complete receive frame has been transferred.   The  
 *             dmaHwi ISR is inserted into the interrupt vector table at  
 *             compile time through a setting in the DSP/BIOS configuration  
 *             under Scheduling --> HWI --> HWI_INT9.  dmaHwi uses the DSP/BIOS  
 *             Dispatcher to save register state and make sure the ISR  
 *             co-exists with other DSP/BIOS functions.  
 */   
void dmaHwi(void)   
{   
    // Ping-pong state.  Initialized to PING initially but declared static so   
    // contents are preserved as dmaHwi() is called repeatedly like a global.   
    static Int16 pingOrPong = PING;   
   
    // Determine if current state is PING or PONG   
    if (pingOrPong == PING) {   
        // Post SWI thread to process PING data   
        SWI_or(&processBufferSwi, PING);   
   
        // Set new state to PONG   
        pingOrPong = PONG;   
    }   
    else {   
        // Post SWI thread to process PONG data   
        SWI_or(&processBufferSwi, PONG);   
   
        // Set new state to PING   
        pingOrPong = PING;   
    }   
   
    // Read the DMA status register to clear it so new interrupts will be seen   
    DMA_RGETH(hDmaRcv, DMACSR);   
       
    count++;   
}   
   
   
/* --------------------------- main() function -------------------------- */   
/*  
 *  main() - The main user task.  Performs application initialization and  
 *           starts the data transfer.  
 */   
void main()   
{   
    Int16 i;   
       
    // Initialize the board support library, must be called first   
    EVM5502_init();   
   
    /* Initialize the LED and DIP switch modules of the BSL */   
    EVM5502_LED_init();   
    EVM5502_DIP_init();    
//    EVM5502_LCD_init();    
       
    // Clear buffers   
    memset((void *)gBufferXmtPing, 0, BUFFSIZE * 4);   
       
    AIC23_setParams(&config);  // Configure the codec   
   
    IRQ_globalDisable();       // Disable global interrupts during setup   
   
    initDma();                 // Initialize the EDMA controller   
   
    initMcbsp();               // Initialize McBSP1 for audio transfers   
   
    if (MCBSP_rrdy(hMcbsp1))   
        MCBSP_read(hMcbsp1);   
    if (MCBSP_xrdy(hMcbsp1))   
        MCBSP_write(hMcbsp1, 0x0);   
   
    initIrq();                 // Initialize interrupts   
       
    IRQ_globalEnable();        // Re-enable global interrupts   
}
