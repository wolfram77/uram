% sampling frequency
Fs = 96 * 10^3;
% start and stop tiems
tStart = 0;
tStop = 0.1;
% info frequencies
F0 = 36 * 10^3;
F1 = 44 * 10^3;
% separator frequency
FX = 40 * 10^3;

% sampling time
Ts = 1 / Fs;
% get capture time for each bit
Tc = 16 / F0;
% get the info and boudary signals
ti = tStart : Ts : Tc;
t = tStart : Ts : tStop;
x_F0 = sin(2*pi* F0 * ti);
x_F1 = sin(2*pi* F1 * ti);
x_FX = sin(2*pi* FX* ti);

% generate the output signal
x_0 = [x_F0 x_F0 x_FX];
x_1 = [x_F0 x_F1 x_FX];
x_2 = [x_F1 x_F0 x_FX];
x_3 = [x_F1 x_F1 x_FX];
x_sig = [x_0 x_0 x_2 x_0 x_1 x_1 x_0 x_3 x_2 x_0 x_3 x_2 x_1 x_1 x_2];
y_sig = 0.01 * awgn(x_sig, 0);
x_len = length(x_sig);

% cross correlate
y_xc0 = abs(xcorr(y_sig, x_0));
y_xc1 = abs(xcorr(y_sig, x_1));
y_xc2 = abs(xcorr(y_sig, x_2));
y_xc3 = abs(xcorr(y_sig, x_3));

% plot results
figure;
subplot(2,2,1);
plot(x_sig);
title('Transmitted signal');
subplot(2,2,2);
plot(y_sig);
title('Recieved Signal');
subplot(2,2,3);
hold on;
plot(x_sig, 'y');
plot(y_xc0(x_len : end), 'k');
plot(y_xc1(x_len : end), 'g');
plot(y_xc2(x_len : end), 'r');
plot(y_xc3(x_len : end), 'b');
title('Detection');
hold off;
