classdef virtuoscope < handle
	
	properties
		FileName = [];
		File = [];
		Mode = [];
		Twait = 0.1;
		Tframe = 0;
		FrameSz = 0;
		Buffer = [];
	end
	
	methods
		function obj = virtuoscope(vso_file, mode, wait_time)
			obj.FileName = vso_file;
			obj.File = fopen(vso_file, mode);
			obj.Mode = mode(1);
			obj.Twait = wait_time;
			if(obj.Mode == 'w')
				fwrite(obj.File, obj.Tframe, 'uint32');
				fwrite(obj.File, obj.FrameSz, 'uint32');
			end
		end
		
		function display(obj, fig_handle, type)
			if(obj.Mode == 'r')
				if(type == 'b')
					while(1)
						obj.display(fig_handle, []);
					end
				else
					fseek(obj.File, 0, 'bof');
					obj.FrameSz = fread(obj.File, [1 2], 'uint32');
					if(obj.FrameSz(1) ~= obj.Tframe)
						obj.Tframe = obj.FrameSz(1);
						obj.FrameSz = obj.FrameSz(2);
						obj.Buffer = fread(obj.File, [1 obj.FrameSz], 'float32');
					end
					figure(fig_handle);
					plot(obj.Buffer);
					title(['Virtuoscope: ' obj.FileName ' (Rx)']);
					drawnow;
					pause(obj.Twait);
				end
			else
				figure(fig_handle);
				plot(obj.Buffer);
				title(['Virtuoscope: ' obj.FileName ' (Tx)']);
				drawnow;
				pause(obj.Twait);
			end
		end
		
		function x_sig = read(obj, type)
			if(obj.Mode == 'r')
				if(type == 'b')
					while(1)
						x_sig = obj.read([]);
						if(~isempty(x_sig))
							break;
						end
						pause(obj.Twait);
					end
				else
					fseek(obj.File, 0, 'bof');
					obj.FrameSz = fread(obj.File, [1 2], 'uint32');
					if(obj.FrameSz(1) ~= obj.Tframe)
						obj.Tframe = obj.FrameSz(1);
						obj.FrameSz = obj.FrameSz(2);
						obj.Buffer = fread(obj.File, [1 obj.FrameSz], 'float32');
						x_sig = obj.Buffer;
					else
						x_sig = [];
					end
				end
			end
		end
		
		function write(obj, frame)
			if(obj.Mode == 'w')
				obj.Buffer = frame;
				obj.Tframe = [(obj.Tframe + 1), length(frame)];
				fseek(obj.File, 0, 'bof');
				fwrite(obj.File, obj.Tframe, 'uint32');
				obj.FrameSz = obj.Tframe(2);
				obj.Tframe = obj.Tframe(1);
				fwrite(obj.File, obj.Buffer, 'float32');
			end
		end
	end
end
