%
% Virtuoscope Test
%
% Produces a moving sine wave output, which is sent to a .vso file and
% displayed, and then samples from the same .vso file are retrieved and
% displayed. This completes the self-test of virtuoscope.
%
% File format (.vso):
% [uint32: time/count value]       -  must change from frame to frame
% [uint32: frame size]             -  frame which is displayed
% [float32: s]
%

clc;
clear all;
close all;

Fs = 8 * 10^3;
tStart = 0;
tDel = 0.01;
Ts = 1 / Fs;

Tx = virtuoscope('test.vso', 'w', 0.0);
Rx = virtuoscope('test.vso', 'r', 0.0);

fig = figure;
while(ishandle(fig))
	t = tStart : Ts : (tStart + tDel);
	tStart = tStart + tDel;
	x_sig = sin(2*pi* 205 * t);
	Tx.write(x_sig);
	subplot(2,1,1);
	Tx.display(fig, []);
	y_sig = Rx.read('b');
	subplot(2,1,2);
	Rx.display(fig, []);
end