classdef virtuoscope < handle
	
	properties
		FileName = [];
		File = [];
		Mode = [];
		Twait = 0;
        MaxFrames = 0;
		FrameSz = 0;
        Rear = 0;
        Front = 0;
		Buffer = [];
	end
	
	methods
        % Initializes a new virtuoscope (in Tx['w'] for Rx['r'] mode)
		function obj = virtuoscope(vso_file, mode, wait_time)
			obj.FileName = vso_file;
			obj.File = fopen(vso_file, mode);
			obj.Mode = mode(1);
			obj.Twait = wait_time;
			if(obj.Mode == 'w')
                obj.MaxFrames = 256;
                temp = [obj.MaxFrames obj.FrameSz obj.Front obj.Rear];
				fwrite(obj.File, temp, 'uint32');
			end
        end
		
        % Update display from buffer
        function updateDisplay(obj, fig_handle)
            figure(fig_handle);
            plot(obj.Buffer);
            if(obj.Mode == 'r')
                title(['Virtuoscope: ' obj.FileName ' (Rx)']);
            else
                title(['Virtuoscope: ' obj.FileName ' (Tx)']);
            end
            drawnow;
            pause(obj.Twait);
        end
        
        % Updates Header info from file
        function updateInfo(obj)
            fseek(obj.File, 0, 'bof');
            temp = fread(obj.File, [1 4], 'uint32');
            if(length(temp) < 4)
                temp = [temp zeros(1, 4 - length(temp))];
            end
            if(obj.MaxFrames == 0)
                obj.MaxFrames = temp(1);
            elseif(obj.MaxFrames ~= temp(1) && temp(1) ~= 0)
                error('Virtuoscope: Max. frames changed during operation. This is not allowed.');
            end
            if(obj.FrameSz == 0)
                obj.FrameSz = temp(2);
            elseif(obj.FrameSz ~= temp(2) && temp(2) ~= 0)
                error('Virtuoscope: Frame Size changed during operation. This is not allowed.');
            end
            obj.Rear = temp(3);
            obj.Front = temp(4);
        end
        
        % Pops a frame from file (if available)
        function x_sig = popToBuffer(obj)
            obj.updateInfo();
            if(obj.Front ~= obj.Rear)
                fseek(obj.File, (4 + (obj.Front * obj.FrameSz)) * 4, 'bof');
                obj.Buffer = fread(obj.File, [1 obj.FrameSz], 'float32');
                obj.Front = mod(obj.Front + 1, obj.MaxFrames);
                fseek(obj.File, 3 * 4, 'bof');
                fwrite(obj.File, obj.Front, 'float32');
                x_sig = obj.Buffer;
            else
                x_sig = [];
            end
        end
        
        % Displays a plot from a frame (type(d) = only display)
		function display(obj, fig_handle, type)
			if(obj.Mode == 'r')
				if(type == 'b')
					while(1)
						obj.display(fig_handle, []);
					end
                else
                    if(type ~= 'd')
                        obj.popToBuffer();
                    end
                    obj.updateDisplay(fig_handle);
				end
            else
                obj.updateDisplay(fig_handle);
			end
        end
		
        % Reads a frame from file
		function x_sig = read(obj, type)
            if(obj.Mode == 'r')
                if(type ~= 'b')
                    x_sig = obj.popToBuffer();
                else
                    while(1)
                        x_sig = obj.popToBuffer();
                        if(~isempty(x_sig))
                            break;
                        end
                        pause(obj.Twait);
                    end
                end
            end
        end
		
        % Writes a frame to file
		function ret = write(obj, frame)
			if(obj.Mode == 'w')
                obj.updateInfo();
                if(obj.FrameSz ~= 0 && length(frame) ~= obj.FrameSz)
                    error('Virtuoscope: Frame Size changed during operation. This is not allowed.');
                end
                obj.FrameSz = length(frame);
                obj.Rear = mod(obj.Rear + 1, obj.MaxFrames);
                if(obj.Rear ~= obj.Front)
                    obj.Buffer = frame;
                    temp = [obj.FrameSz obj.Rear];
                    fseek(obj.File, 2 * 4, 'bof');
                    fwrite(obj.File, temp, 'uint32');
                    fseek(obj.File, (4 + obj.Rear * obj.FrameSz) * 4, 'bof');
                    fwrite(obj.File, obj.Buffer, 'float32');
                    ret = 0;
                else
                    ret = -1;
                end
			end
		end
	end
end
